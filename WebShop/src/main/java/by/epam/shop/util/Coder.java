package by.epam.shop.util;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * This class consists exclusively of static methods which provide secure
 * password hashing.
 */
public class Coder {

	private static final SecureRandom random = new SecureRandom();

	/* base of hashed password number */
	private static final int radix = 32;

	/* amounts of bits, which will be randomly generated */
	private static final int bits = 130;

	/**
	 * Calculates hash of concatenated password and salt via sha256.
	 * 
	 * @param password
	 * @param salt
	 */
	public static String getHash(String password, String salt) {
		String hash = DigestUtils.sha256Hex(password + salt);
		return hash;
	}

	/**
	 * Generates random string via cryptographically strong random number
	 * generator.
	 */
	public static String generateSalt() {
		return new BigInteger(bits, random).toString(radix);
	}
}

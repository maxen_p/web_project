<%@ page language="java" contentType="text/html; charset=UTF-8"
		pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/WEB-INF/jsp/jspf/bundle.jspf"%>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><fmt:message key="title.main" /></title>
	<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
  	<script src="${pageContext.request.contextPath}/js/jquery-1.12.4.min.js"></script>
	<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
  			rel="stylesheet">
	<link href="${pageContext.request.contextPath}/css/main.css"
  			rel="stylesheet">
	<link href="${pageContext.request.contextPath}/css/items.css"
  			rel="stylesheet">
</head>
<body>
	<%@include file="/WEB-INF/jsp/jspf/ind_nav.jspf" %>
	<%@include file="/WEB-INF/jsp/jspf/message.jspf"%>

 <div class="container">
	<c:if test="${empty sessionScope.basket}">
		<h3 align="center" id="message">
        	<fmt:message key="message.basket.no_orders" />
        </h3>
	</c:if>
	<div class="row">
		<c:forEach var="item" items="${sessionScope.basket}">
        	<div class="col-sm-4">
            	<div class="panel panel-primary">
                	<div class="panel-heading">${item.itemName}</div>
                    <div class="panel-body">
                    	<p><img src="${item.itemPicPath}"class="img-responsive" alt="Image"/></p>
                  		<p><fmt:message key="info.item.id"/> ${item.itemId}</p>
                  		<p><fmt:message key="info.item.amount_shop"/> ${item.itemsAmount}</p>
                  		<p><fmt:message key="info.item.amount_order"/> ${sessionScope.amount_map[item.itemId]}</p>
                  		<p><fmt:message key="info.item.type"/> ${item.itemType}</p>         
                  		<p><fmt:message key="info.item.price"/> ${item.price}</p>
                       	<fmt:message key="info.item.desc"/>
                		<div class="descc">
							<p> ${item.itemDescription}</p>
						</div>
                	</div> 
                	<c:choose>
            			<c:when test="${not empty sessionScope.basket}">
                			<form action="Controller" method="post">
                  				<input class="btn btn-block btn-primary" type="submit" name="order_items" 
                  						value="<fmt:message key="button.order_items" />" /> 
                  				<input type="hidden" name="basket" value="${sessionScope.basket}" /> 
                  				<input type="hidden" name="client" value="${sessionScope.client}" />
                  				<input type="hidden" name="command" value="order_items" />
                			</form>
            			</c:when>
        				<c:otherwise>
        					<h3 align="center" id="message">
        						<fmt:message key="message.basket.no_orders" />
      						</h3>
    					</c:otherwise>
  					</c:choose>
                	<form action="Controller" method="post">
                  		<input class="btn btn-block btn-primary" type="submit" value="<fmt:message key="button.item.delete" />" /> 
                  		<input type="hidden" name="item_id" value="${item.itemId}" />
                  		<input type="hidden" name="basket" value="${sessionScope.basket}" /> 
                  		<input type="hidden" name="basket" value="${sessionScope.amount_map}" /> 
                  		<input type="hidden" name="amount" value="${item.itemsAmount}" />
                  		<input type="hidden" name="command" value="delete_item_from_basket" />
                	</form>
				</div> 
			</div> 
		</c:forEach>                  
	</div>   
</div>
</body>
</html>

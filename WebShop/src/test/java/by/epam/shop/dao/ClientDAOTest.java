package by.epam.shop.dao;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import by.epam.shop.dao.impl.UserDAOImpl;
import by.epam.shop.dao.pool.DBConnectionPool;
import by.epam.shop.entity.User;
import by.epam.shop.exception.ConnectionPoolException;
import by.epam.shop.exception.DAOException;
import by.epam.shop.util.Coder;

public class ClientDAOTest {

    private static final Logger LOG = Logger.getLogger(ClientDAOTest.class);
    private static UserDAO clientDAO = UserDAOImpl.getInstance();

    private static User client = new User();

    @BeforeClass
    public static void initialize() {
        try {
            DBConnectionPool.getInstance().init();

            client.setName("Richard");
            client.setLogin("ppiper");
            client.setSurname("Hendricks");
            client.setStatus(0);
            client.setStatusBL(0);
            client.setPassword(Coder.getHash("hACk33rr", Coder.generateSalt()));
            client.setId(Math.abs(client.hashCode()));

        } catch (ConnectionPoolException e) {
            throw new RuntimeException("Error while initializing connection pool", e);
        }
    }

    @Before
    public void testSignUp() {
        LOG.info("testSignUp()");

        try {
            clientDAO.create(client);
            String login = client.getLogin();
            Assert.assertNotNull(login);
            String password = client.getPassword();
            Assert.assertNotNull(password);

            User newClient = clientDAO.findEntity(login);
            Assert.assertEquals(client.getLogin(), newClient.getLogin());
            Assert.assertEquals(client.getName(), newClient.getName());
            Assert.assertEquals(client.getSurname(), newClient.getSurname());
            Assert.assertEquals(client.getStatus(), newClient.getStatus());
            Assert.assertEquals(client.getStatus(), newClient.getStatusBL());
            Assert.assertEquals(client.getPassword(), newClient.getPassword());
            Assert.assertEquals(client.getId(), newClient.getId());
        } catch (DAOException e) {
            LOG.error("DAOException", e);
        }
        return;
    }

    @Test
    public void testFindClientByLogin() {
        LOG.info("testFindClientByLoginAndPassword()");

        try {
            User newClient = clientDAO.findEntity("ppiper");

            Assert.assertEquals(client.getName(), newClient.getName());
            Assert.assertEquals(client.getSurname(), newClient.getSurname());
            Assert.assertEquals(client.getStatus(), newClient.getStatus());
            Assert.assertEquals(client.getStatusBL(), newClient.getStatusBL());
        } catch (DAOException e) {
            LOG.error("DAOException", e);
        }
    }

    @Test
    public void testChangeBlackListStatus() {
        LOG.info("testChangeBlackListStatus()");

        try {
            String login = client.getLogin();
            Assert.assertNotNull(login);
            String password = client.getPassword();
            Assert.assertNotNull(password);
            int statusBL = client.getStatusBL();
            Assert.assertNotNull(statusBL);
            clientDAO.changeBlackListStatus(login, 1);

            User newClient = clientDAO.findEntity(login);
            Assert.assertFalse(client.getStatusBL() == newClient.getStatusBL());
        } catch (DAOException e) {
            LOG.error("DAOException", e);
        }
        return;
    }

    @After
    public void testDeleteClient() {
        LOG.info("testDeleteClient()");

        try {
            String login = client.getLogin();
            Assert.assertNotNull(login);
            String password = client.getPassword();
            Assert.assertNotNull(password);

            clientDAO.delete(login);
            User newClient = clientDAO.findEntity(login);
            Assert.assertEquals(null, newClient);
        } catch (DAOException e) {
            LOG.error("DAOException", e);
        }
        return;
    }

    @AfterClass
    public static void destroyPool() {
        try {
            DBConnectionPool.getInstance().destroy();
        } catch (ConnectionPoolException e) {
            LOG.error(e);
        }
    }

}

package by.epam.shop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLStreamException;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import by.epam.shop.command.Command;
import by.epam.shop.command.factory.Factory;
import by.epam.shop.resource.ConfigurationManager;
import by.epam.shop.resource.MessageManager;

/**
 * Processes all incoming requests by redirecting them to the corresponding
 * command.
 */
@SuppressWarnings("serial")
@WebServlet("/Controller")
public class Controller extends HttpServlet {
	private static final Logger LOG = Logger.getLogger(Controller.class);

	private static final Factory factory = new Factory();
	private static final String INDEX_PAGE = "path.page.index";
	private static final String MESSAGE = "message";

	public Controller() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (XMLStreamException | SAXException e) {
			LOG.error(e);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (XMLStreamException | SAXException e) {
			LOG.error(e);
		}
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, XMLStreamException, SAXException {
		LOG.info("Controller.processRequest()");
		String page = null;
		Command command = null;
		try {
			command = factory.defineCommand(request);
		} catch (IllegalArgumentException e) {
			LOG.error(e);
			request.setAttribute(MESSAGE, MessageManager.UNRECOGNIZED_COMMAND);
			page = ConfigurationManager.getProperty(INDEX_PAGE);
		}
		page = command.execute(request);

		if (page != null) {
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
			dispatcher.forward(request, response);
		} else {
			page = ConfigurationManager.getProperty(INDEX_PAGE);
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
			dispatcher.forward(request, response);
		}
	}
}

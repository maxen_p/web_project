package by.epam.shop.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import by.epam.shop.dao.pool.DBConnectionPool;
import by.epam.shop.exception.ConnectionPoolException;

/**
 * Initializes connections to the database before application start, closes
 * connections on application stop.
 */
public class ShopServletContextListener implements ServletContextListener {
	private static final Logger LOG = Logger.getLogger(ShopServletContextListener.class);

	@Override
	public void contextInitialized(ServletContextEvent event) {
		LOG.info("ShopServletContextListener.contextInit()");
		try {
			DBConnectionPool.getInstance().init();
		} catch (ConnectionPoolException e) {
			throw new RuntimeException("Error while initializing connection pool", e);
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		LOG.info("ShopServletContextListener.contextDestroyed()");
		try {
			DBConnectionPool.getInstance().destroy();
		} catch (ConnectionPoolException e) {
			LOG.error(e);
		}
	}

}

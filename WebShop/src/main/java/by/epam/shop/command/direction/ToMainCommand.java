package by.epam.shop.command.direction;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.shop.command.Command;
import by.epam.shop.resource.ConfigurationManager;

public class ToMainCommand implements Command {

	private static Logger LOG = Logger.getLogger(ToIndexCommand.class);
	private final String URL = "url";
	private final String MAIN_PAGE = "path.page.main";

	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("ToIndexCommand.execute()");

		String page = ConfigurationManager.getProperty(MAIN_PAGE);
		request.getSession().setAttribute(URL, page);
		return page;
	}
}

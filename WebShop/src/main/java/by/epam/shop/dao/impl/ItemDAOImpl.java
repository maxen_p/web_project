package by.epam.shop.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.shop.dao.ItemDAO;
import by.epam.shop.dao.pool.DBConnectionPool;
import by.epam.shop.entity.Item;
import by.epam.shop.exception.ConnectionPoolException;
import by.epam.shop.exception.DAOException;

public class ItemDAOImpl implements ItemDAO {

	private static final Logger LOG = Logger.getLogger(ItemDAOImpl.class);
	private static final String ITEM_ID = "item.id";
	private static final String ITEM_NAME = "item.name";
	private static final String ITEM_AMOUNT = "item.amount";
	private static final String ITEM_PRICE = "item.price";
	private static final String ITEM_DESC = "item.description";
	private static final String ITEM_PATH = "item.path";
	private static final String ITEM_TYPE = "item.type";
	private static final String SQL_SELECT_ALL_ITEMS = "SELECT * FROM webshop.item";
	private static final String SQL_SELECT_LIMITED_ITEMS = "SELECT * FROM webshop.item limit ?, ?";
	private static final String SQL_CALC_ITEMS = "SELECT count(*) FROM webshop.item";
	private static final String SQL_SELECT_ITEMS_BY_TYPE = "SELECT * FROM webshop.item WHERE item.type =?";
	private static final String SQL_SELECT_ITEMS_BY_ID = "SELECT * FROM webshop.item WHERE item.id =?";
	private static final String SQL_CREATE_ITEM = "INSERT INTO webshop.item(id, name, amount, price, description, path, type) VALUES(?,?,?,?,?,?,?)";
	private static final String SQL_DELETE_ITEM = "DELETE FROM webshop.item WHERE item.id = ?";
	private static final String SQL_MODIFY_ITEM_INFO = "UPDATE webshop.item SET name = ?, amount = ?, price = ?, description = ?, path = ?, type = ? WHERE id = ?";
	private static final String SQL_SELECT_ITEMS_BY_ORDER_ID = "SELECT * FROM (webshop.item JOIN webshop.item_order ON item.id = item_order.item_id) WHERE item_order.order_id = ?";
	private static ItemDAOImpl instance = new ItemDAOImpl();

	private ItemDAOImpl() {
	}

	public static ItemDAO getInstance() {
		return instance;
	}

	@Override
	public List<Item> findAll() throws DAOException {
		LOG.info("ItemDAOImpl.findAll()");

		DBConnectionPool pool = DBConnectionPool.getInstance();

		ArrayList<Item> items = new ArrayList<Item>();
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.giveConnection();
			prStatement = connection.prepareStatement(SQL_SELECT_ALL_ITEMS);
			ResultSet resultSet = prStatement.executeQuery();
			while (resultSet.next()) {
				Item item = new Item();
				item.setItemId(resultSet.getInt(ITEM_ID));
				item.setItemName(resultSet.getString(ITEM_NAME));
				item.setItemsAmount(resultSet.getInt(ITEM_AMOUNT));
				item.setPrice(resultSet.getString(ITEM_PRICE));
				item.setItemDescription(resultSet.getString(ITEM_DESC));
				item.setItemPicPath(resultSet.getString(ITEM_PATH));
				item.setItemType(resultSet.getString(ITEM_TYPE));
				items.add(item);
			}
		} catch (SQLException | ConnectionPoolException e) {
			LOG.info(e);
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
					prStatement.close();
				}
				pool.takeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
				throw new DAOException(e);
			}
		}
		return items;
	}

	public List<Item> findLimited(int offset, int amount) throws DAOException {
		LOG.info("ItemDAOImpl.findAll()");

		DBConnectionPool pool = DBConnectionPool.getInstance();

		ArrayList<Item> items = new ArrayList<Item>();
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.giveConnection();
			prStatement = connection.prepareStatement(SQL_SELECT_LIMITED_ITEMS);
			prStatement.setInt(1, offset);
			prStatement.setInt(2, amount);
			ResultSet resultSet = prStatement.executeQuery();
			while (resultSet.next()) {
				Item item = new Item();
				item.setItemId(resultSet.getInt(ITEM_ID));
				item.setItemName(resultSet.getString(ITEM_NAME));
				item.setItemsAmount(resultSet.getInt(ITEM_AMOUNT));
				item.setPrice(resultSet.getString(ITEM_PRICE));
				item.setItemDescription(resultSet.getString(ITEM_DESC));
				item.setItemPicPath(resultSet.getString(ITEM_PATH));
				item.setItemType(resultSet.getString(ITEM_TYPE));
				items.add(item);
			}
			resultSet.close();
		} catch (SQLException | ConnectionPoolException e) {
			LOG.info(e);
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
					prStatement.close();
				}
				pool.takeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
				throw new DAOException(e);
			}
		}
		return items;
	}

	public int calcNumberOfItems() throws DAOException {
		LOG.info("ItemDAOImpl.calcNumberOfItems()");

		DBConnectionPool pool = DBConnectionPool.getInstance();

		int numberOfItems = 0;
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.giveConnection();
			prStatement = connection.prepareStatement(SQL_CALC_ITEMS);
			ResultSet resultSet = prStatement.executeQuery();
			if (resultSet.next()) {
				numberOfItems = resultSet.getInt(1);
			}
			resultSet.close();
		} catch (SQLException | ConnectionPoolException e) {
			LOG.info(e);
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
					prStatement.close();
				}
				pool.takeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
				throw new DAOException(e);
			}
		}
		return numberOfItems;
	}

	public List<Item> findItemsByType(String type) throws DAOException {
		DBConnectionPool pool = DBConnectionPool.getInstance();

		ArrayList<Item> items = new ArrayList<>();
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.giveConnection();
			prStatement = connection.prepareStatement(SQL_SELECT_ITEMS_BY_TYPE);
			prStatement.setString(1, type);
			ResultSet resultSet = prStatement.executeQuery();
			while (resultSet.next()) {
				Item item = new Item();
				item.setItemId(resultSet.getInt(ITEM_ID));
				item.setItemName(resultSet.getString(ITEM_NAME));
				item.setItemsAmount(resultSet.getInt(ITEM_AMOUNT));
				item.setPrice(resultSet.getString(ITEM_PRICE));
				item.setItemDescription(resultSet.getString(ITEM_DESC));
				item.setItemPicPath(resultSet.getString(ITEM_PATH));
				item.setItemType(resultSet.getString(ITEM_TYPE));
				items.add(item);
			}
		} catch (SQLException | ConnectionPoolException e) { // 2exs
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
					prStatement.close();
				}
				pool.takeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
				throw new DAOException(e);
			}
		}
		return items;
	}

	@Override
	public Item findEntity(Integer id) throws DAOException {
		DBConnectionPool pool = DBConnectionPool.getInstance();

		Item item = null;
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.giveConnection();
			prStatement = connection.prepareStatement(SQL_SELECT_ITEMS_BY_ID);
			prStatement.setInt(1, id);
			ResultSet resultSet = prStatement.executeQuery();
			while (resultSet.next()) {
				item = new Item();
				item.setItemId(resultSet.getInt(ITEM_ID));
				item.setItemName(resultSet.getString(ITEM_NAME));
				item.setItemsAmount(resultSet.getInt(ITEM_AMOUNT));
				item.setPrice(resultSet.getString(ITEM_PRICE));
				item.setItemDescription(resultSet.getString(ITEM_DESC));
				item.setItemPicPath(resultSet.getString(ITEM_PATH));
				item.setItemType(resultSet.getString(ITEM_TYPE));
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
					prStatement.close();
				}
				pool.takeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
				throw new DAOException(e);
			}
		}
		return item;
	}

	@Override
	public boolean create(Item item) throws DAOException {
		DBConnectionPool pool = DBConnectionPool.getInstance();

		boolean result = false;
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			// !!!
			connection = pool.giveConnection();

			LOG.info(item.toString());
			prStatement = connection.prepareStatement(SQL_CREATE_ITEM);
			prStatement.setInt(1, item.getItemId());
			prStatement.setString(2, item.getItemName());
			prStatement.setInt(3, item.getItemsAmount());
			prStatement.setString(4, item.getPrice());
			prStatement.setString(5, item.getItemDescription());
			prStatement.setString(6, item.getItemPicPath());
			prStatement.setString(7, item.getItemType());
			if (prStatement.executeUpdate() > 0) {
				result = true;
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
					prStatement.close();
				}
				pool.takeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
				throw new DAOException(e);
			}
		}
		return result;
	}

	@Override
	public boolean delete(Integer itemId) throws DAOException {
		DBConnectionPool pool = DBConnectionPool.getInstance();

		boolean result = false;
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.giveConnection();
			prStatement = connection.prepareStatement(SQL_DELETE_ITEM);
			prStatement.setInt(1, itemId);

			if (prStatement.executeUpdate() > 0) {
				result = true;
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
					prStatement.close();
				}
				pool.takeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
				throw new DAOException(e);
			}
		}
		return result;
	}

	public boolean modifyItemInfo(Item item) throws DAOException {
		DBConnectionPool pool = DBConnectionPool.getInstance();

		boolean result = false;
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.giveConnection();
			prStatement = connection.prepareStatement(SQL_MODIFY_ITEM_INFO);

			prStatement.setString(1, item.getItemName());
			prStatement.setInt(2, item.getItemsAmount());
			prStatement.setString(3, item.getPrice());
			prStatement.setString(4, item.getItemDescription());
			prStatement.setString(5, item.getItemPicPath());
			prStatement.setString(6, item.getItemType());
			prStatement.setInt(7, item.getItemId());
			if (prStatement.executeUpdate() > 0) {
				result = true;
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException();
		} finally {
			try {
				if (prStatement != null) {
					prStatement.close();
				}
				pool.takeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
				throw new DAOException(e);
			}
		}
		return result;
	}

	public List<Item> findItemsByOrderId(int id) throws DAOException {
		LOG.info("findItemsByOrderAndId()");

		DBConnectionPool pool = DBConnectionPool.getInstance();

		ArrayList<Item> items = new ArrayList<>();
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.giveConnection();
			prStatement = connection.prepareStatement(SQL_SELECT_ITEMS_BY_ORDER_ID);
			prStatement.setInt(1, id);
			ResultSet resultSet = prStatement.executeQuery();

			while (resultSet.next()) {
				Item item = new Item();
				item.setItemId(resultSet.getInt(ITEM_ID));
				item.setItemName(resultSet.getString(ITEM_NAME));
				item.setItemType(resultSet.getString(ITEM_TYPE));
				item.setItemsAmount(resultSet.getInt(ITEM_AMOUNT));
				item.setItemDescription(resultSet.getString(ITEM_DESC));
				item.setItemPicPath(resultSet.getString(ITEM_PATH));
				item.setPrice(resultSet.getString(ITEM_PRICE));

				items.add(item);
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
					prStatement.close();
				}
				pool.takeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
				throw new DAOException(e);
			}
		}
		return items;
	}

}

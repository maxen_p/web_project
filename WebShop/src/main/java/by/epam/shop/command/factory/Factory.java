package by.epam.shop.command.factory;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.shop.command.Command;
import by.epam.shop.command.enumeration.Commands;
import by.epam.shop.command.impl.EmptyCommand;

/**
 * Defines commands based on request scope property "command".
 */
public class Factory {

	private static final Logger LOG = Logger.getLogger(Factory.class);
	private final String COMMAND = "command";

	public Command defineCommand(HttpServletRequest request) throws IllegalArgumentException {
		LOG.info("Factory.defineCommand()");

		String action = request.getParameter(COMMAND);
		if (action == null || action.isEmpty()) {
			return new EmptyCommand();
		}
		Commands current = Commands.valueOf(action.toUpperCase());
		Command command = current.getCurrentCommand();
		return command;
	}
}

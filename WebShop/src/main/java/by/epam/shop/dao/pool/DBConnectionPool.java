package by.epam.shop.dao.pool;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;

import org.apache.log4j.Logger;

import by.epam.shop.dao.resource.DBParameter;
import by.epam.shop.dao.resource.DBResourceManager;
import by.epam.shop.exception.ConnectionPoolException;

/**
 * This class is a cache of database connections maintained so that the
 * connections can be reused when future requests to the database are required.
 */
public class DBConnectionPool {

	private static final Logger LOG = Logger.getLogger(DBConnectionPool.class);

	/**
	 * Parameter required to initialize connections to the database.
	 */
	private static final String DRIVER = DBResourceManager.getValue(DBParameter.DB_DRIVER);
	private static final String URL = DBResourceManager.getValue(DBParameter.DB_URL);
	private static final String USER = DBResourceManager.getValue(DBParameter.DB_USER);
	private static final String PASSWORD = DBResourceManager.getValue(DBParameter.DB_PASSWORD);

	/**
	 * Number of active database connections contained in pool.
	 */
	private static final int SIZE = Integer.parseInt(DBResourceManager.getValue(DBParameter.DB_POOL_SIZE));

	/**
	 * This class is a singleton, it's the only instance of connection pool,
	 * available via getInstance() method.
	 */
	private static DBConnectionPool instance = new DBConnectionPool();

	/**
	 * Thread save queue of unused connections.
	 */
	private BlockingQueue<ConnectionWrapper> availableConnections;

	/**
	 * Thread save queue of used connections.
	 */
	private BlockingQueue<ConnectionWrapper> usedConnections;

	private DBConnectionPool() {
		try {
			Class.forName(DRIVER);
			availableConnections = new ArrayBlockingQueue<>(SIZE);
			usedConnections = new ArrayBlockingQueue<>(SIZE);
		} catch (Exception e) {
			LOG.error(e);
		}
	}

	public static DBConnectionPool getInstance() {
		return instance;
	}

	/**
	 * Initializes available connections to the database.
	 * 
	 * @throws ConnectionPoolException
	 */
	public void init() throws ConnectionPoolException {
		LOG.info("init()");

		for (int i = 0; i < SIZE; i++) {
			Connection connection = getConnection();
			ConnectionWrapper connectionWrapper = new ConnectionWrapper(connection);
			availableConnections.add(connectionWrapper);
		}
	}

	/**
	 * Establishes connection to the database.
	 * 
	 * @return active connection
	 * @throws ConnectionPoolException
	 */
	private Connection getConnection() throws ConnectionPoolException {
		Connection connection;
		try {
			connection = DriverManager.getConnection(URL, USER, PASSWORD);
		} catch (SQLException e) {
			throw new ConnectionPoolException(e);
		}
		return connection;
	}

	/**
	 * Gives database connection, if one is available.
	 * 
	 * @return Connection
	 * @throws ConnectionPoolException
	 */
	public Connection giveConnection() throws ConnectionPoolException {
		ConnectionWrapper newConnection;
		try {
			newConnection = availableConnections.take();
			usedConnections.put(newConnection);
		} catch (InterruptedException e) {
			throw new ConnectionPoolException(e);
		}
		return newConnection;
	}

	/**
	 * Returns connection back to unused(available) connections of connection
	 * pool.
	 * 
	 * @param connection
	 * @throws ConnectionPoolException
	 */
	public void takeConnection(Connection connection) throws ConnectionPoolException {
		LOG.info("takeConnection()");
		LOG.info("Connection to take: " + connection);
		LOG.info("Is contained in used connections? " + usedConnections.contains(connection));

		try {
			if (connection != null) {
				if (usedConnections.remove(connection)) {
					availableConnections.put((ConnectionWrapper) connection);
				} else {
					throw new ConnectionPoolException("Connection isn't contained in the usedConnections");
				}
			}
		} catch (InterruptedException e) {
			throw new ConnectionPoolException(e);
		}
		LOG.info("available conenctions after closeConnection:" + availableConnections.size());
		LOG.info("used connections after closeConnection:" + usedConnections.size());
	}

	/**
	 * Closes all connections contained in connection pool.
	 * 
	 * @throws ConnectionPoolException
	 */
	public void destroy() throws ConnectionPoolException {
		LOG.info("destroy()");
		LOG.info("available connections before destroy:" + availableConnections.size());
		LOG.info("used connections before destroy:" + usedConnections.size());

		try {
			closeConnectionQueue(availableConnections);
		} catch (ConnectionPoolException e) {
			throw new ConnectionPoolException("Can't correctly destroy connection pool.");
		} finally {
			closeConnectionQueue(usedConnections);
		}
		LOG.info("av.con. after destroy:" + availableConnections.size());
		LOG.info("used.con. after destroy:" + usedConnections.size());
	}

	/**
	 * Closes all connections contained in queue.
	 * 
	 * @param queue
	 * @throws ConnectionPoolException
	 */
	private void closeConnectionQueue(BlockingQueue<ConnectionWrapper> queue) throws ConnectionPoolException {
		LOG.info("closeConnectionQueue()");

		for (Connection connection : queue) {
			try {
				if (connection != null) {
					((ConnectionWrapper) connection).dispose();
				}
			} catch (SQLException e) {
				throw new ConnectionPoolException("Can't close connection queue.");
			}
		}
	}

	private class ConnectionWrapper implements Connection {

		private Connection connection;

		public ConnectionWrapper(Connection connection) {
			this.connection = connection;
		}

		@Override
		public Statement createStatement() throws SQLException {
			return connection.createStatement();
		}

		@Override
		public PreparedStatement prepareStatement(String sql) throws SQLException {
			return connection.prepareStatement(sql);
		}

		@Override
		public CallableStatement prepareCall(String sql) throws SQLException {
			return connection.prepareCall(sql);
		}

		@Override
		public String nativeSQL(String sql) throws SQLException {
			return connection.nativeSQL(sql);
		}

		@Override
		public void setAutoCommit(boolean autoCommit) throws SQLException {
			connection.setAutoCommit(autoCommit);
		}

		@Override
		public boolean getAutoCommit() throws SQLException {
			return connection.getAutoCommit();
		}

		@Override
		public void commit() throws SQLException {
			connection.commit();
		}

		@Override
		public void rollback() throws SQLException {
			connection.rollback();
		}

		/**
		 * Does not close connection, but returns it to available connections of
		 * the connection pool.
		 */
		@Override
		public void close() throws SQLException {
			LOG.info("close()");
			LOG.info("Connection to close():" + connection);

			if (connection.isClosed()) {
				LOG.info("Trying to close connection, which is already closed!!!!");
				return;
			}
			try {
				takeConnection(connection);
			} catch (ConnectionPoolException e) {
				LOG.error(e);
			}
		}

		private void dispose() throws SQLException {
			LOG.info("WrapperConnection.dispose()");
			this.connection.close();
		}

		@Override
		public boolean isClosed() throws SQLException {
			return connection.isClosed();
		}

		@Override
		public DatabaseMetaData getMetaData() throws SQLException {
			return connection.getMetaData();
		}

		@Override
		public void setReadOnly(boolean readOnly) throws SQLException {
			connection.setReadOnly(readOnly);
		}

		@Override
		public boolean isReadOnly() throws SQLException {
			return connection.isReadOnly();
		}

		@Override
		public void setCatalog(String catalog) throws SQLException {
			connection.setCatalog(catalog);
		}

		@Override
		public String getCatalog() throws SQLException {
			return connection.getCatalog();
		}

		@Override
		public void setTransactionIsolation(int level) throws SQLException {
			connection.setTransactionIsolation(level);
		}

		@Override
		public int getTransactionIsolation() throws SQLException {
			return connection.getTransactionIsolation();
		}

		@Override
		public SQLWarning getWarnings() throws SQLException {
			return connection.getWarnings();
		}

		@Override
		public void clearWarnings() throws SQLException {
			connection.clearWarnings();
		}

		@Override
		public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
			return connection.createStatement(resultSetType, resultSetConcurrency);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency)
				throws SQLException {
			return connection.prepareStatement(sql, resultSetType, resultSetConcurrency);
		}

		@Override
		public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency)
				throws SQLException {
			return connection.prepareCall(sql, resultSetType, resultSetConcurrency);
		}

		@Override
		public Map<String, Class<?>> getTypeMap() throws SQLException {
			return connection.getTypeMap();
		}

		@Override
		public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
			connection.setTypeMap(map);
		}

		@Override
		public void setHoldability(int holdability) throws SQLException {
			connection.setHoldability(holdability);
		}

		@Override
		public int getHoldability() throws SQLException {
			return connection.getHoldability();
		}

		@Override
		public Savepoint setSavepoint() throws SQLException {
			return connection.setSavepoint();
		}

		@Override
		public Savepoint setSavepoint(String name) throws SQLException {
			return connection.setSavepoint(name);
		}

		@Override
		public void rollback(Savepoint savepoint) throws SQLException {
			connection.rollback();
		}

		@Override
		public void releaseSavepoint(Savepoint savepoint) throws SQLException {
			connection.releaseSavepoint(savepoint);
		}

		@Override
		public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability)
				throws SQLException {
			return connection.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency,
				int resultSetHoldability) throws SQLException {
			return connection.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
		}

		@Override
		public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency,
				int resultSetHoldability) throws SQLException {
			return connection.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
			return connection.prepareStatement(sql, autoGeneratedKeys);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
			return connection.prepareStatement(sql, columnIndexes);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
			return connection.prepareStatement(sql, columnNames);
		}

		@Override
		public Clob createClob() throws SQLException {
			return connection.createClob();
		}

		@Override
		public Blob createBlob() throws SQLException {
			return connection.createBlob();
		}

		@Override
		public NClob createNClob() throws SQLException {
			return connection.createNClob();
		}

		@Override
		public SQLXML createSQLXML() throws SQLException {
			return connection.createSQLXML();
		}

		@Override
		public boolean isValid(int timeout) throws SQLException {
			return connection.isValid(timeout);
		}

		@Override
		public void setClientInfo(String name, String value) throws SQLClientInfoException {
			connection.setClientInfo(name, value);
		}

		@Override
		public void setClientInfo(Properties properties) throws SQLClientInfoException {
			connection.setClientInfo(properties);
		}

		@Override
		public String getClientInfo(String name) throws SQLException {
			return connection.getClientInfo(name);
		}

		@Override
		public Properties getClientInfo() throws SQLException {
			return connection.getClientInfo();
		}

		@Override
		public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
			return connection.createArrayOf(typeName, elements);
		}

		@Override
		public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
			return connection.createStruct(typeName, attributes);
		}

		@Override
		public void setSchema(String schema) throws SQLException {
			connection.setSchema(schema);
		}

		@Override
		public String getSchema() throws SQLException {
			return connection.getSchema();
		}

		@Override
		public void abort(Executor executor) throws SQLException {
			connection.abort(executor);
		}

		@Override
		public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
			connection.setNetworkTimeout(executor, milliseconds);
		}

		@Override
		public int getNetworkTimeout() throws SQLException {
			return connection.getNetworkTimeout();
		}

		@Override
		public <T> T unwrap(Class<T> iface) throws SQLException {
			return connection.unwrap(iface);
		}

		@Override
		public boolean isWrapperFor(Class<?> iface) throws SQLException {
			return connection.isWrapperFor(iface);
		}
	}

}

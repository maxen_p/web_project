package by.epam.shop.util;

import java.util.regex.Pattern;

import by.epam.shop.entity.User;
import by.epam.shop.entity.Item;
import by.epam.shop.resource.MessageManager;

/**
 * This class validates user's input.
 */
public class Validator {

	private static final Pattern LOGIN = Pattern.compile("^[a-z0-9_-]{3,15}$");
	private static final Pattern PASSWORD = Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,32})");
	private static final Pattern NAME_SURNAME = Pattern.compile("[А-Яа-я\\w]{2,50}");
	private static final Pattern AMOUNT = Pattern.compile("[0-9]{1,4}");
	private static final Pattern ITEM_NAME = Pattern.compile("[\\S\\s]{1,255}");
	private static final Pattern DESCRIPTION = Pattern.compile("[\\S\\s]{1,2000}");
	private static final Pattern PRICE = Pattern.compile("[\\d\\s.,_]{1,30}[$pр]");

	public static String checkClientInfo(User client, String pass) {
		if (!LOGIN.matcher(client.getLogin()).matches()) {
			return MessageManager.INCORRECT_LOGIN;
		}
		if (!PASSWORD.matcher(pass).matches()) {
			return MessageManager.INCORRECT_PASSWORD;
		}
		if (!NAME_SURNAME.matcher(client.getName()).matches() && !NAME_SURNAME.matcher(client.getSurname()).matches()) {
			return MessageManager.INCORRECT_NAME_SURNAME;
		}
		return null;
	}

	public static String checkItemInfo(Item item) {
		if (!ITEM_NAME.matcher(item.getItemName()).matches()) {
			return MessageManager.INCORRECT_NAME_ITEM;
		}
		if (!AMOUNT.matcher(String.valueOf(item.getItemsAmount())).matches()) {
			return MessageManager.INCORRECT_AMOUNT;
		}
		if (!DESCRIPTION.matcher(String.valueOf(item.getItemDescription())).matches()) {
			return MessageManager.INCORRECT_DESC;
		}
		if (!PRICE.matcher(item.getPrice()).matches()) {
			return MessageManager.INCORRECT_PRICE;
		}

		return null;
	}

	public static boolean checkEmptyField(String... args) {
		if (args.length == 0) {
			return false;
		} else {
			for (String arg : args) {
				if (arg.length() == 0) {
					return false;
				}
			}
		}
		return true;
	}
}

package by.epam.shop.command.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.shop.command.Command;
import by.epam.shop.dao.OrderDAO;
import by.epam.shop.dao.impl.OrderDAOImpl;
import by.epam.shop.entity.Order;
import by.epam.shop.exception.DAOException;
import by.epam.shop.resource.ConfigurationManager;
import by.epam.shop.resource.MessageManager;

public class FinishOrderCommand implements Command {

	private static Logger LOG = Logger.getLogger(FinishOrderCommand.class);
	private final String URL = "url";
	private final String ORDER_ID = "order_id";
	private final String ORDERS = "orders";
	private final String MESSAGE = "message";
	private final String ORDER_PAGE = "path.page.order.admin";
	private final String ERROR_PAGE = "path.page.error";

	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("FinishOrderCommand.execute()");

		String page = null;
		List<Order> orders = (List<Order>) request.getSession().getAttribute(ORDERS);
		String orderId = request.getParameter(ORDER_ID);
		OrderDAO orderDAO = OrderDAOImpl.getInstance();
		try {
			for (Order order : orders) {
				if (order.getOrderId() == Integer.parseInt(orderId)) {
					if (orderDAO.updateOrderStatus(order.getOrderId(), order.getStatus())) {
						orders = orderDAO.findAll();
						request.getSession().setAttribute(ORDERS, orders);
						request.setAttribute(MESSAGE, MessageManager.SUCCESSFUL_CHANGE_STATUS_ORDER);
					} else {
						request.setAttribute(MESSAGE, MessageManager.FINISH_ORDER_ERROR);
					}
				}
			}
		} catch (DAOException e) {
			LOG.error(e);
			request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
			page = ConfigurationManager.getProperty(ERROR_PAGE);
		}
		request.getSession().setAttribute(ORDERS, orders);
		page = ConfigurationManager.getProperty(ORDER_PAGE);
		request.getSession().setAttribute(URL, page);
		return page;
	}

}

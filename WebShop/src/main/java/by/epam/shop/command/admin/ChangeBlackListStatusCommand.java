package by.epam.shop.command.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.shop.command.Command;
import by.epam.shop.dao.UserDAO;
import by.epam.shop.dao.impl.UserDAOImpl;
import by.epam.shop.entity.User;
import by.epam.shop.exception.DAOException;
import by.epam.shop.resource.ConfigurationManager;
import by.epam.shop.resource.MessageManager;

public class ChangeBlackListStatusCommand implements Command {

	private static final Logger LOG = Logger.getLogger(ChangeBlackListStatusCommand.class);

	private final String LOGIN = "login";
	private final String STATUS_BL = "status_BL";
	private final String STATUS = "status";
	private final String MESSAGE = "message";
	private final String ERROR_PAGE = "path.page.error";
	private final String CLIENTS_ADMIN_PAGE = "path.page.admin.take_clients";
	private final String URL = "url";
	private final String CLIENTS = "clients";

	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("ChangeBlackListStatusCommand.execute()");

		String page = null;
		String login = request.getParameter(LOGIN);
		int status = Integer.parseInt(request.getParameter(STATUS));
		int statusBL = Integer.parseInt(request.getParameter(STATUS_BL));

		LOG.info("login: " + login);
		LOG.info("status_BL :" + statusBL);

		UserDAO clientDAO = UserDAOImpl.getInstance();
		try {
			List<User> clients = clientDAO.findAll();
			if (status == 1) {
				request.setAttribute(MESSAGE, MessageManager.ERROR_CHANGE_ADMIN_BL_STATUS);
				page = ConfigurationManager.getProperty(CLIENTS_ADMIN_PAGE);
			} else {
				if (statusBL == 0) {
					if (clientDAO.changeBlackListStatus(login, 1)) {
						request.setAttribute(MESSAGE, MessageManager.SUCCESSFUL_BLACKLIST_STATUS_CHANGE);
						request.setAttribute(CLIENTS, clientDAO.findAll());
						page = ConfigurationManager.getProperty(CLIENTS_ADMIN_PAGE);
					} else {
						request.setAttribute(MESSAGE, MessageManager.CHANGE_BLACKLIST_STATUS_ERROR);
						request.setAttribute(CLIENTS, clients);
						page = ConfigurationManager.getProperty(CLIENTS_ADMIN_PAGE);
					}
				} else if (statusBL == 1) {
					if (clientDAO.changeBlackListStatus(login, 0)) {
						request.setAttribute(MESSAGE, MessageManager.SUCCESSFUL_BLACKLIST_STATUS_CHANGE);
						request.setAttribute(CLIENTS, clientDAO.findAll());
						page = ConfigurationManager.getProperty(CLIENTS_ADMIN_PAGE);
					} else {
						request.setAttribute(MESSAGE, MessageManager.CHANGE_BLACKLIST_STATUS_ERROR);
						request.setAttribute(CLIENTS, clients);
						page = ConfigurationManager.getProperty(CLIENTS_ADMIN_PAGE);
					}
				}
			}
		} catch (DAOException e) {
			LOG.info("DAOException", e);
			request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
			page = ConfigurationManager.getProperty(ERROR_PAGE);
		}
		request.getSession().setAttribute(URL, page);
		return page;
	}

}

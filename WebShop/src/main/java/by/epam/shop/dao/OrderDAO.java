package by.epam.shop.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import by.epam.shop.entity.User;
import by.epam.shop.entity.Order;
import by.epam.shop.exception.DAOException;

public interface OrderDAO extends GenericDAO<Integer, Order> {

	/**
	 * Returns a list of all orders from data source.
	 */
	public List<Order> findAll() throws DAOException;

	/**
	 * Searches for an order by it's identifier.
	 */
	public Order findEntity(Integer id) throws DAOException;

	/**
	 * Deletes an order from the data source.
	 */
	public boolean delete(Integer id) throws DAOException;

	/**
	 * Saves an order in data source.
	 */
	public boolean create(Order order) throws DAOException;

	/**
	 * Finds all user's orders.
	 * 
	 * @param login
	 * @param client
	 * @return list of orders
	 * @throws DAOException
	 */
	public ArrayList<Order> takeOrderByLogin(String login, User client) throws DAOException;

	/**
	 * Checks if any order contains item.
	 * 
	 * @param itemId
	 * @return true if item is contained in any order, otherwise false.
	 * @throws DAOException
	 */
	public boolean findOrdersByItemId(int itemId) throws DAOException;

	/**
	 * Attempts to delete items from the order.
	 * 
	 * @param orderId
	 * @return true if items are successfully deleted, otherwise false.
	 * @throws DAOException
	 */
	public boolean deleteItemsFromOrders(int orderId) throws DAOException;

	/**
	 * Puts item and ordered amount in amountMap, if finds any
	 * 
	 * @param orderId
	 * @param amountMap
	 * @return true if successfully finds items in order
	 * @throws DAOException
	 */
	public boolean getIOAmountAndItemId(int orderId, HashMap<Integer, Integer> amountMap) throws DAOException;

	/**
	 * Tries to add items to order.
	 * 
	 * @param order
	 * @return
	 * @throws DAOException
	 */
	public boolean addItemsToOrder(Order order) throws DAOException;

	/**
	 * Updates order status.
	 * 
	 * @param orderId
	 * @param status
	 * @return
	 * @throws DAOException
	 */
	public boolean updateOrderStatus(int orderId, int status) throws DAOException;

	/**
	 * Checks if order exists
	 * 
	 * @param id
	 * @return true if order exists, otherwise false.
	 * @throws DAOException
	 */
	public boolean isExist(int id) throws DAOException;

}

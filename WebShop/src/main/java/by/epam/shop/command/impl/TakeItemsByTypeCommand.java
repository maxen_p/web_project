package by.epam.shop.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.shop.command.Command;
import by.epam.shop.dao.ItemDAO;
import by.epam.shop.dao.impl.ItemDAOImpl;
import by.epam.shop.entity.Item;
import by.epam.shop.exception.DAOException;
import by.epam.shop.resource.ConfigurationManager;
import by.epam.shop.resource.MessageManager;

public class TakeItemsByTypeCommand implements Command {
	private static final Logger LOG = Logger.getLogger(TakeItemsByTypeCommand.class);
	private final String MESSAGE = "message";
	private final String URL = "url";
	private final String ITEMS = "items";
	private final String ITEM_TYPE = "item_type";
	private final String ERROR_PAGE = "path.page.error";
	private final String ITEMS_PAGE = "path.page.items";

	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("TakeItemsByTypeCommand.execute()");

		String page = null;
		String type = request.getParameter(ITEM_TYPE);
		ItemDAO itemDAO = ItemDAOImpl.getInstance();
		try {
			List<Item> items = itemDAO.findItemsByType(type);
			if (items.isEmpty()) {
				request.setAttribute(MESSAGE, MessageManager.SEARCHING_ITEM_NOT_FOUND);
				request.getSession().setAttribute(ITEMS, null);
				page = ConfigurationManager.getProperty(ITEMS_PAGE);
			} else {
				request.getSession().setAttribute(ITEMS, items);
				page = ConfigurationManager.getProperty(ITEMS_PAGE);
			}
		} catch (DAOException e) {
			LOG.error("DAOException", e);
			request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
			page = ConfigurationManager.getProperty(ERROR_PAGE);
		}

		request.getSession().setAttribute(URL, page);
		return page;
	}

}

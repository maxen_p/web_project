package by.epam.shop.tag.custom;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * This custom tag welcomes user after log in.
 */
@SuppressWarnings("serial")
public class ClientInfoTag extends TagSupport {

	private final String EN_WELCOME = "Welcome, ";
	private final String RU_WELCOME = "Добро пожаловать, ";
	private String name;
	private String surname;
	private String locale;

	public void setName(String name) {
		this.name = name;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	@Override
	public int doStartTag() throws JspException {
		try {
			if (locale.equalsIgnoreCase("ru")) {
				pageContext.getOut().write(RU_WELCOME + name + " " + surname + "!");
			} else {
				pageContext.getOut().write(EN_WELCOME + name + " " + surname + "!");
			}
		} catch (IOException e) {
			throw new JspException(e.getMessage());
		}
		return SKIP_BODY;
	}
}
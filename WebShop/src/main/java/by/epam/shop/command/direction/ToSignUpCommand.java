package by.epam.shop.command.direction;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.shop.command.Command;
import by.epam.shop.resource.ConfigurationManager;

public class ToSignUpCommand implements Command {

	private static final Logger LOG = Logger.getLogger(ToSignUpCommand.class);
	private final String URL = "url";
	private final String SIGN_UP_PAGE = "path.page.signup";

	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("ToSignUpCommand.execute()");

		String page = ConfigurationManager.getProperty(SIGN_UP_PAGE);
		request.getSession().setAttribute(URL, page);
		return page;
	}
}

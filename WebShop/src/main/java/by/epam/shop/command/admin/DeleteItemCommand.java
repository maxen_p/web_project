package by.epam.shop.command.admin;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.shop.command.Command;
import by.epam.shop.dao.ItemDAO;
import by.epam.shop.dao.OrderDAO;
import by.epam.shop.dao.impl.ItemDAOImpl;
import by.epam.shop.dao.impl.OrderDAOImpl;
import by.epam.shop.entity.Item;
import by.epam.shop.exception.DAOException;
import by.epam.shop.resource.ConfigurationManager;
import by.epam.shop.resource.MessageManager;

public class DeleteItemCommand implements Command {

	private static Logger LOG = Logger.getLogger(DeleteItemCommand.class);
	private final String MESSAGE = "message";
	private final String URL = "url";
	private final String ITEM_ID = "item_id";
	private final String BASKET = "basket";
	private final String ERROR_PAGE = "path.page.error";
	private final String ITEMS_PAGE = "path.page.items";

	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest request) {
		LOG.debug("DeleteItemCommand execute()");

		String page = null;
		int itemId = Integer.parseInt(request.getParameter(ITEM_ID));
		ArrayList<Item> basket = (ArrayList<Item>) request.getSession().getAttribute(BASKET);
		ItemDAO itemDAO = ItemDAOImpl.getInstance();
		OrderDAO orderDAO = OrderDAOImpl.getInstance();
		if (basket != null) {
			for (Item item : basket) {
				if (item.getItemId() == itemId) {
					request.setAttribute(MESSAGE, MessageManager.ERROR_DELETE_ITEM);
					page = ConfigurationManager.getProperty(ITEMS_PAGE);
					request.getSession().setAttribute(URL, page);
					return page;
				}
			}
		}
		try {
			if (!orderDAO.findOrdersByItemId(itemId)) {
				if (itemDAO.delete(itemId)) {
					request.setAttribute(MESSAGE, MessageManager.DELETE_ITEM_SUCCESS);
					page = ConfigurationManager.getProperty(ITEMS_PAGE);

				} else {
					request.setAttribute(MESSAGE, MessageManager.ERROR_DELETE_ITEM);
					page = ConfigurationManager.getProperty(ITEMS_PAGE);
				}
			} else {
				request.setAttribute(MESSAGE, MessageManager.ERROR_DELETE_ITEM);
				page = ConfigurationManager.getProperty(ITEMS_PAGE);
			}
		} catch (DAOException e) {
			LOG.error("DAOException", e);
			request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
			page = ConfigurationManager.getProperty(ERROR_PAGE);
		}
		request.getSession().setAttribute(URL, page);
		return page;
	}
}

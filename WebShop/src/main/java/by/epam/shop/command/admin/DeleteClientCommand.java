package by.epam.shop.command.admin;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.shop.command.Command;
import by.epam.shop.dao.UserDAO;
import by.epam.shop.dao.OrderDAO;
import by.epam.shop.dao.impl.UserDAOImpl;
import by.epam.shop.dao.impl.OrderDAOImpl;
import by.epam.shop.entity.User;
import by.epam.shop.entity.Order;
import by.epam.shop.exception.DAOException;
import by.epam.shop.resource.ConfigurationManager;
import by.epam.shop.resource.MessageManager;

public class DeleteClientCommand implements Command {

	private static final Logger LOG = Logger.getLogger(DeleteClientCommand.class);
	private final String MESSAGE = "message";
	private final String URL = "url";
	private final String LOGIN = "login";
	private final String STATUS = "status";
	private final String ADMIN_TAKE_CLIENTS_PAGE = "path.page.admin.take_clients";
	private final String ERROR_PAGE = "path.page.error";

	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("DeleteClientCommand.execute()");

		String page = null;
		String login = request.getParameter(LOGIN);
		int status = Integer.parseInt(request.getParameter(STATUS));
		User client = null;

		UserDAO clientDAO = UserDAOImpl.getInstance();
		OrderDAO orderDAO = OrderDAOImpl.getInstance();
		try {
			ArrayList<Order> orders = orderDAO.takeOrderByLogin(login, client);

			if (!clientDAO.exists(login)) {
				LOG.info("not exist");
				request.setAttribute(MESSAGE, MessageManager.DELETE_DELETED_CLIENT_ERROR);
				page = ConfigurationManager.getProperty(ADMIN_TAKE_CLIENTS_PAGE);
			} else {
				LOG.info("exist");
				if (status == 1) {
					request.setAttribute(MESSAGE, MessageManager.ERROR_DELETE_ADMIN);
					page = ConfigurationManager.getProperty(ADMIN_TAKE_CLIENTS_PAGE);
				} else if (!orders.isEmpty()) {
					request.setAttribute(MESSAGE, MessageManager.ERROR_DELETE_CLIENT_HAVE_ORDER);
					page = ConfigurationManager.getProperty(ADMIN_TAKE_CLIENTS_PAGE);
				} else if (clientDAO.delete(login)) {
					request.setAttribute(MESSAGE, MessageManager.DELETE_CLIENT_SUCCESS);
					page = ConfigurationManager.getProperty(ADMIN_TAKE_CLIENTS_PAGE);
				} else {
					request.setAttribute(MESSAGE, MessageManager.ERROR_DELETE_CLIENT);
					page = ConfigurationManager.getProperty(ADMIN_TAKE_CLIENTS_PAGE);
				}
			}
		} catch (DAOException e) {
			LOG.error(e);
			request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
			page = ConfigurationManager.getProperty(ERROR_PAGE);
		}

		request.getSession().setAttribute(URL, page);
		return page;
	}

}

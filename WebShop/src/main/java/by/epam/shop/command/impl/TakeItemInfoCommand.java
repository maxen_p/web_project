package by.epam.shop.command.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.shop.command.Command;
import by.epam.shop.dao.ItemDAO;
import by.epam.shop.dao.impl.ItemDAOImpl;
import by.epam.shop.entity.Item;
import by.epam.shop.exception.DAOException;
import by.epam.shop.resource.ConfigurationManager;
import by.epam.shop.resource.MessageManager;

public class TakeItemInfoCommand implements Command {

	private static final Logger LOG = Logger.getLogger(TakeItemInfoCommand.class);

	private final String MESSAGE = "message";
	private final String URL = "url";
	private final String ITEM = "item";
	private final String ITEM_ID = "item_id";
	private final String ITEM_INFO_PAGE = "path.page.item_info";
	private final String ERROR_PAGE = "path.page.error";

	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("TakeItemInfoCommand.execute()");

		String page = null;
		int itemId = Integer.parseInt(request.getParameter(ITEM_ID));
		ItemDAO itemDAO = ItemDAOImpl.getInstance();
		try {
			Item item = itemDAO.findEntity(itemId);
			if (item != null) {
				request.getSession().setAttribute(ITEM, item);
				page = ConfigurationManager.getProperty(ITEM_INFO_PAGE);
			} else {
				request.setAttribute(MESSAGE, MessageManager.TAKE_ITEM_INFO_ERROR);
				page = ConfigurationManager.getProperty(ERROR_PAGE);
			}
		} catch (DAOException e) {
			LOG.error("DAOException", e);
			request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
			page = ConfigurationManager.getProperty(ERROR_PAGE);
		}
		request.getSession().setAttribute(URL, page);
		return page;
	}
}

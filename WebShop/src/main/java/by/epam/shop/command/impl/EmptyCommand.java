package by.epam.shop.command.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.shop.command.Command;
import by.epam.shop.resource.ConfigurationManager;

public class EmptyCommand implements Command {
	private static final Logger log = Logger.getLogger(EmptyCommand.class);
	private static final String MAIN_PAGE = "path.page.main";

	@Override
	public String execute(HttpServletRequest request) {
		log.info("EmptyCommand.execute()");

		String page = ConfigurationManager.getProperty(MAIN_PAGE);
		return page;
	}
}
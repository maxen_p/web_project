package by.epam.shop.command.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.shop.exception.DAOException;
import by.epam.shop.command.Command;
import by.epam.shop.dao.UserDAO;
import by.epam.shop.dao.impl.UserDAOImpl;
import by.epam.shop.entity.User;
import by.epam.shop.resource.ConfigurationManager;
import by.epam.shop.resource.MessageManager;
import by.epam.shop.util.Coder;
import by.epam.shop.util.Validator;

public class SignUpCommand implements Command {
	private static final Logger LOG = Logger.getLogger(SignUpCommand.class);
	private final String LOGIN = "login";
	private final String PASSWORD = "password";
	private final String NAME = "name";
	private final String SURNAME = "surname";
	private final String CLIENT = "client";
	private final String MESSAGE = "message";
	private final String URL = "url";
	private final String SIGNUP_PAGE = "path.page.signup";
	private final String MAIN_PAGE = "path.page.main";
	private final String ERROR_PAGE = "path.page.error";

	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("SignUpCommand.execute()");

		String page = null;
		try {
			UserDAO clientDAO = UserDAOImpl.getInstance();
			
			String login = request.getParameter(LOGIN).trim();
			String password = request.getParameter(PASSWORD).trim();
			String salt = Coder.generateSalt();
			String name = request.getParameter(NAME).trim();
			String surname = request.getParameter(SURNAME).trim();

			if (!Validator.checkEmptyField(login, password, name, surname)) {
				request.setAttribute(MESSAGE, MessageManager.SIGNUP_ERROR);
				page = ConfigurationManager.getProperty(SIGNUP_PAGE);
				return page;
			}

			String hashedPass = Coder.getHash(password, salt);
			
			User client = new User(login, hashedPass, salt, name, surname, 0, 0);
			client.setId(Math.abs(client.hashCode()));

			String check = Validator.checkClientInfo(client, password);
			LOG.info(check);

			if (check == null) {
				boolean result = clientDAO.create(client);

				if (!result) {
					request.setAttribute(MESSAGE, MessageManager.SIGNUP_EXIST_ERROR);
					page = ConfigurationManager.getProperty(SIGNUP_PAGE);
				} else {
					request.getSession().setAttribute(CLIENT, client);
					request.setAttribute(MESSAGE, MessageManager.SIGNUP_SUCCESS);
					page = ConfigurationManager.getProperty(MAIN_PAGE);
				}
			} else {
				request.setAttribute(MESSAGE, check);
				page = ConfigurationManager.getProperty(SIGNUP_PAGE);
			}
		} catch (DAOException e) {
			LOG.info("DAOException", e);
			request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
			page = ConfigurationManager.getProperty(ERROR_PAGE);
			return page;
		}
		request.getSession().setAttribute(URL, page);
		return page;
	}
}
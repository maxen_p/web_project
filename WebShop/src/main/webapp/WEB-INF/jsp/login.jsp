<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/WEB-INF/jsp/jspf/bundle.jspf"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><fmt:message key="title.login"/></title>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/login.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet">
</head>
<body>
	<%@include file="/WEB-INF/jsp/jspf/login_nav.jspf"%>  
    <%@include file="/WEB-INF/jsp/jspf/message.jspf"%>
    
<div class="container">
	<form id="form-signin" class="form-signin" name="LogInForm" method="POST" action="Controller">
       	<h2 class="form-signin-heading"><fmt:message key="static.login"/></h2>
        <input type="text"  class="form-control" placeholder="<fmt:message key="login"/>" name="login"
        		pattern="^[a-z0-9_-]{3,15}$" 
				title="Must contain at least 3 characters with any lower case character, digit or special symbol “_-” only" 
				required>
        <input type="password" class="form-control" placeholder="<fmt:message key="password"/>" name="password" 
        		pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,32}" 
				title="Must contain at least one number, one uppercase and lowercase letter, and at least 6 or more characters" 
				required>
    	<input type="hidden" name="command" value="login"/>
        <input class="btn btn-lg btn-success btn-block" type="submit" value="<fmt:message key="logIn"/>">
	</form>
	<form class="form-signin" name="ToSignUpForm" method="POST" action="Controller">
    	<input type="hidden" name="command" value="tosignup"/>
    	<input class="btn btn-lg btn-primary btn-block" type="submit" value="<fmt:message key="toSignUp"/> "/>
    </form>
</div> 
</body>
</html>
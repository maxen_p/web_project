package by.epam.shop.command.admin;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.shop.command.Command;
import by.epam.shop.dao.ItemDAO;
import by.epam.shop.dao.impl.ItemDAOImpl;
import by.epam.shop.entity.Item;
import by.epam.shop.exception.DAOException;
import by.epam.shop.resource.ConfigurationManager;
import by.epam.shop.resource.MessageManager;
import by.epam.shop.util.Validator;

public class AddItemCommand implements Command {
	private static final Logger LOG = Logger.getLogger(AddItemCommand.class);

	private final String URL = "url";
	private final String NAME = "name";
	private final String TYPE = "type";
	private final String AMOUNT = "amount";
	private final String DESCRIPTION = "description";
	private final String PATH = "path";
	private final String PRICE = "price";
	private final String MESSAGE = "message";
	private final String ITEM = "item";
	private final String ADD_ITEM_PAGE = "path.page.admin.add_item";

	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("AddItemCommand.execute()");

		String page = null;
		String itemName = request.getParameter(NAME).trim();
		String type = request.getParameter(TYPE).trim();
		int amount = Integer.parseInt(request.getParameter(AMOUNT).trim());
		String description = request.getParameter(DESCRIPTION).trim();
		String path = request.getParameter(PATH).trim();
		String price = request.getParameter(PRICE).trim();

		if (!Validator.checkEmptyField(itemName, price, description, path, itemName)) {
			request.setAttribute(MESSAGE, MessageManager.ADD_ITEM_ERROR);
			page = ConfigurationManager.getProperty(ADD_ITEM_PAGE);
			request.getSession().setAttribute(URL, page);
			return page;
		}

		Item item = new Item();
		item.setItemName(itemName);
		item.setItemsAmount(amount);
		item.setPrice(price);
		item.setItemPicPath(path);
		item.setItemDescription(description);
		item.setItemType(type);
		item.setItemId(Math.abs(item.hashCode()));

		String check = Validator.checkItemInfo(item);
		ItemDAO itemDAO = ItemDAOImpl.getInstance();
		try {
			if (check == null) {
				if (itemDAO.findEntity(item.getItemId()) == null) {
					if (itemDAO.create(item)) {
						request.setAttribute(ITEM, item);
						page = ConfigurationManager.getProperty(ADD_ITEM_PAGE);
						request.setAttribute(MESSAGE, MessageManager.SUCCESFUL_ITEM_ADD_BY_ADMIN);
					} else {
						request.setAttribute(MESSAGE, MessageManager.ADD_ITEM_BY_ADMIN_ERROR);
						page = ConfigurationManager.getProperty(ADD_ITEM_PAGE);
					}
				} else {
					request.setAttribute(MESSAGE, MessageManager.ADDING_ITEM_EXISTS_ERROR);
					page = ConfigurationManager.getProperty(ADD_ITEM_PAGE);
				}
			} else {
				request.setAttribute(MESSAGE, MessageManager.ILLEGAL_ITEM_INPUT_DATA);
				page = ConfigurationManager.getProperty(ADD_ITEM_PAGE);
			}
		} catch (DAOException e) {
			LOG.error("DAOException", e);
			request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
			page = ConfigurationManager.getProperty(ADD_ITEM_PAGE);
		}

		request.getSession().setAttribute(URL, page);
		return page;
	}
}

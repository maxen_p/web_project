<%@ page language="java" contentType="text/html; charset=UTF-8"
		pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/WEB-INF/jsp/jspf/bundle.jspf"%>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><fmt:message key="title.main" /></title>
	<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
  	<script src="${pageContext.request.contextPath}/js/jquery-1.12.4.min.js"></script>
	<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
  			rel="stylesheet">
	<link href="${pageContext.request.contextPath}/css/main.css"
  			rel="stylesheet">
</head>
<body>
	<%@include file="/WEB-INF/jsp/jspf/ind_nav.jspf" %>
  	<%@include file="/WEB-INF/jsp/jspf/message.jspf"%>

<div class="container">
	<div class="row">
    	<div class="col-sm-4">
        	<div class="panel panel-primary">
          		<div class="panel-heading"><fmt:message key="static.add.modify_item"/></div>
          		<div class="panel-body">
            		<form id="modifyform"  method="POST" action="Controller">
    					<div class="form-group">
      						<fmt:message key="static.add.item.name"/>
      						<input type="text" class="form-control" id="name" 
      								name="name" value="${item.itemName}" required
      								pattern="[\S\s]{1,255}"
									title="Must contain from 1 to 255 symbols, may contain whitespaces.">
    					</div>
    					<div class="form-group">
     						<fmt:message key="static.add.item.type"/>
      						<select class="form-control" id="type" name="type" required>
       							<option value="1">1</option>
        						<option value="2">2</option>
        						<option value="3">3</option>
        						<option value="4">4</option>
        						<option value="5">5</option>
      						</select>
    					</div>
    					<div class="form-group">
      						<fmt:message key="static.add.item.amount"/>
      						<input type="text" class="form-control" id="amount" 
      								name="amount" value="${item.itemsAmount}" required
      								pattern="[0-9]{1,4}"
									title="Must contain from 1 to 4 digits">
    					</div>
    					<div class="form-group">
      						<fmt:message key="static.add.item.desc"/>
      						<textarea class="form-control" rows="3" id="desc" 
      								name="description" required>${item.itemDescription}</textarea>
    					</div>
    					<div class="form-group">
      						<fmt:message key="static.add.item.pic"/>
      						<input type="text" class="form-control" id="path" 
      								name="path" value="${item.itemPicPath}" required>
    					</div>
    					<div class="form-group">
      						<fmt:message key="static.add.item.price"/>
      						<input type="text" class="form-control" id="manufacter" 
      								name="manufacter" value="${item.price}" required
      								pattern="[\d\s.,_]{1,30}[$pр]"
									title="May contain whitespaces, commas, dots, underlines. Maximum of 30 symbols, must contain digits, $ or р required.">
    					</div>
    					<input type="hidden" name="item_id" value="${item.itemId}" />
    					<input type="hidden" name="command" value="modify_item"/>
    					<input class="btn btn-primary btn-block" type="submit" 
    							value="<fmt:message key="button.item.modify"/> "/>
    				</form>
        		</div>
      		</div>
		</div>
    </div>
</div>
</body>
</html>

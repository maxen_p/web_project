package by.epam.shop.command.direction;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.shop.command.Command;
import by.epam.shop.resource.ConfigurationManager;

public class ToAddItemCommand implements Command {

	private static final Logger LOG = Logger.getLogger(ToAddItemCommand.class);
	private final String URL = "url";
	private final String ADMIN_ADD_ITEM_PAGE = "path.page.admin.add_item";

	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("ToAddItemCommand.execute()");
		String page = null;
		page = ConfigurationManager.getProperty(ADMIN_ADD_ITEM_PAGE);
		request.getSession().setAttribute(URL, page);
		return page;
	}

}

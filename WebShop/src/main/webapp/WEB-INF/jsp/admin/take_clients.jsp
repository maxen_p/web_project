<%@ page language="java" contentType="text/html; charset=UTF-8"
		pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/WEB-INF/jsp/jspf/bundle.jspf"%>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><fmt:message key="title.main" /></title>
	<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
  	<script src="${pageContext.request.contextPath}/js/jquery-1.12.4.min.js"></script>
	<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
  			rel="stylesheet">
	<link href="${pageContext.request.contextPath}/css/main.css"
  			rel="stylesheet">
</head>
<body>
	<%@include file="/WEB-INF/jsp/jspf/ind_nav.jspf" %>
    <%@include file="/WEB-INF/jsp/jspf/message.jspf"%>
  
<div class="container">             
	<div class="row">
		<c:forEach var="client" items="${clients}">
			<div class="col-sm-4" >
				<div class="panel panel-primary">
					<div class="panel-heading">
                    	<fmt:message key="info.client.login"/> ${client.login}
					</div>
          			<div class="panel-body">
                  		<p><fmt:message key="info.client.id"/> ${client.id}</p> 
                  		<p><fmt:message key="info.client.name"/> ${client.name}</p>
                  		<p><fmt:message key="info.client.surname"/> ${client.surname}</p>
                  		<p><fmt:message key="info.client.status"/> ${client.status}</p>
                  		<p><fmt:message key="info.client.statusBL"/> ${client.statusBL}</p>
                		<c:choose>
                  			<c:when test="${client.status == 1}">
                    			<p><fmt:message key="static.status.admin"/><p>
                  			</c:when>
                  			<c:otherwise>
                    			<p><fmt:message key="static.status.notAdmin"/></p>
                  			</c:otherwise>
                		</c:choose>
                		
                		<form class="form" name="AddToBlackListForm" method="POST" action="Controller">
	                  		<input type="hidden" name="command" value="blacklist_client"/>
                  			<input type="hidden" name="status_BL" value="${client.statusBL}"/>
                  			<input type="hidden" name="status" value="${client.status}"/>
                  			<input type="hidden" name="login" value="${client.login}"/>
                  			<input class="btn btn-primary" type="submit" value="<fmt:message key="button.blacklist"/> "/>
                		</form>                
                		<form class="form" name="DeleteClientForm" method="POST" action="Controller">
                  			<input type="hidden" name="command" value="delete_client"/>
                  			<input type="hidden" name="login" value="${client.login}"/>
                  			<input type="hidden" name="status" value="${client.status}"/>
                  			<input class="btn btn-danger" type="submit" value="<fmt:message key="button.delete"/> "/>
                		</form>      
                	</div>
                </div>    
            </div> 
      	</c:forEach> 
    </div>                     
</div>
</body>
</html>

Project Description  
This web shop project is build on Servlet+JSP technologies with  
MySQL as data storage. Dependencies are managed by Maven.  

Requirements  
JDK 1.8+, Maven 3+, MySQL 5.6+  

Building and Running  
Import database schema from webshop_structure.sql, then import data from webshop_data.sql.  
Configure db.properties located in WebShop/src/main/resources:  
	1. db.url=url_to_db (default: jdbc:mysql://localhost:3306/webshop)  
	2. db.user=database_user  
	3. db.password=user_password  
Compile and run web application on port 8080:  
	mvn jetty:run  

Further Steps  
In browser open http://localhost:8080/  
Create new account or login as admin via username: admin, password: Admin1  


package by.epam.shop.dao.resource;

import java.util.ResourceBundle;

/**
 * Provides access to the database configuration parameters via keys.
 */
public class DBResourceManager {

	private static final String PROPERTY_FILE = "db";
	private static final DBResourceManager INSTANCE = new DBResourceManager();

	private static ResourceBundle bundle = ResourceBundle.getBundle(PROPERTY_FILE);

	public static DBResourceManager getInstance() {
		return INSTANCE;
	}

	public static String getValue(String key) {
		return bundle.getString(key);
	}
}

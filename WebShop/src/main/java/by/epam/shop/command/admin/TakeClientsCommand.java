package by.epam.shop.command.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.shop.command.Command;
import by.epam.shop.dao.UserDAO;
import by.epam.shop.dao.impl.UserDAOImpl;
import by.epam.shop.entity.User;
import by.epam.shop.exception.DAOException;
import by.epam.shop.resource.ConfigurationManager;
import by.epam.shop.resource.MessageManager;

public class TakeClientsCommand implements Command {
	private static final Logger LOG = Logger.getLogger(TakeClientsCommand.class);
	private final String MESSAGE = "message";
	private final String URL = "url";
	private final String CLIENTS = "clients";
	private final String ERROR_PAGE = "path.page.error";
	private final String TAKE_CLIENTS_PAGE = "path.page.admin.take_clients";

	@Override
	public String execute(HttpServletRequest request) {
		String page;
		UserDAO clientDAO = UserDAOImpl.getInstance();
		try {
			List<User> clients = clientDAO.findAll();
			LOG.info(clients.toString());
			if (clients.isEmpty()) {
				request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
				page = ConfigurationManager.getProperty(ERROR_PAGE);
			} else {
				request.getSession().setAttribute(CLIENTS, clients);
				page = ConfigurationManager.getProperty(TAKE_CLIENTS_PAGE);
			}

		} catch (DAOException e) {
			LOG.error("DAOException", e);
			request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
			page = ConfigurationManager.getProperty(ERROR_PAGE);
		}
		request.getSession().setAttribute(URL, page);
		return page;
	}
}
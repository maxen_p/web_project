package by.epam.shop.command.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.shop.command.Command;
import by.epam.shop.dao.UserDAO;
import by.epam.shop.dao.impl.UserDAOImpl;
import by.epam.shop.entity.User;
import by.epam.shop.exception.DAOException;
import by.epam.shop.resource.ConfigurationManager;
import by.epam.shop.resource.MessageManager;
import by.epam.shop.util.Coder;
import by.epam.shop.util.Validator;

public class LogInCommand implements Command {

	private static final Logger LOG = Logger.getLogger(LogInCommand.class);
	private final String LOGIN = "login";
	private final String PASSWORD = "password";
	private final String MESSAGE = "message";
	private final String URL = "url";
	private final String CLIENT = "client";
	private final String LOGIN_PAGE = "path.page.login";
	private final String MAIN_PAGE = "path.page.main";
	private final String ERROR_PAGE = "path.page.error";

	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("LogInCommand.execute()");

		String page = null;
		String login = request.getParameter(LOGIN).trim();
		String password = request.getParameter(PASSWORD).trim();

		if (!Validator.checkEmptyField(login, password)) {
			request.setAttribute(MESSAGE, MessageManager.LOGIN_ERROR);
			page = ConfigurationManager.getProperty(LOGIN_PAGE);
			return page;
		}

		UserDAO clientDAO = UserDAOImpl.getInstance();
		User client = null;
		try {
			client = clientDAO.findEntity(login);
			if (client == null) {
				request.setAttribute(MESSAGE, MessageManager.LOGIN_ERROR);
				page = ConfigurationManager.getProperty(LOGIN_PAGE);
			} else if (client.getStatusBL() == 1) {
				request.setAttribute(MESSAGE, MessageManager.LOGIN_BL_ERROR);
				page = ConfigurationManager.getProperty(LOGIN_PAGE);
			} else if (client.getPassword().equals(Coder.getHash(password, client.getSalt()))) {
				LOG.info(client.toString());
				request.getSession().setAttribute(CLIENT, client);
				page = ConfigurationManager.getProperty(MAIN_PAGE);
			} else {
				request.setAttribute(MESSAGE, MessageManager.LOGIN_ERROR);
				page = ConfigurationManager.getProperty(LOGIN_PAGE);
			}
		} catch (DAOException e) {
			LOG.error("DAOException", e);
			request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
			page = ConfigurationManager.getProperty(ERROR_PAGE);
		}

		request.getSession().setAttribute(URL, page);
		return page;
	}

}

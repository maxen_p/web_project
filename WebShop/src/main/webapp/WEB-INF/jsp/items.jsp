<%@ page language="java" contentType="text/html; charset=UTF-8"
		pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/WEB-INF/jsp/jspf/bundle.jspf"%>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><fmt:message key="title.main" /></title>
	<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
  	<script src="${pageContext.request.contextPath}/js/jquery-1.12.4.min.js"></script>
	<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
  			rel="stylesheet">
	<link href="${pageContext.request.contextPath}/css/main.css"
  		rel="stylesheet">
  		<link href="${pageContext.request.contextPath}/css/items.css"
  		rel="stylesheet">
</head>
<body>
	<%@include file="/WEB-INF/jsp/jspf/ind_nav.jspf" %>
	<%@include file="/WEB-INF/jsp/jspf/message.jspf"%>

<div class="container">
	<div class="row">
		<c:forEach var="item" items="${items}">
			<div class="col-sm-4">
				<div class="panel panel-primary">
					<div class="panel-heading">${item.itemName}</div>
                  	<div class="panel-body">
                    	<p><img src="${item.itemPicPath}" class="img-responsive" alt="Image"/></p>
                		<p><fmt:message key="info.item.id"/> ${item.itemId}</p>
                		<p><fmt:message key="info.item.amount_shop"/> ${item.itemsAmount}</p>
                		<p><fmt:message key="info.item.price"/> ${item.price}</p>
                		<fmt:message key="info.item.desc"/>
                		<div class="descc">
							<p> ${item.itemDescription}</p>
						</div>
                		<form action="Controller" method="post">
							<div class="form-group">
                      			<p><fmt:message key="info.item.amount"/></p>
                      			<select class="form-control" id="amount" name="amount">
                        			<option value="1">1</option>
                        			<option value="2">2</option>
                        			<option value="3">3</option>
                      			</select>
                    		</div>
                    		<input class="btn btn-block btn-primary" type="submit" 
                    				value="<fmt:message key="button.item.add_to_order" />" /> 
							<input type="hidden" name="item_id" value="${item.itemId}" />
                    		<input type="hidden" name="client" value="${sessionScope.client}" /> 
		                    <input type="hidden" name="items" value="${items}" /> 
                    		<input type="hidden" name="basket" value="${sessionScope.basket}" /> 
                    		<input type="hidden" name="amount_map" value="${sessionScope.amount_map}" />
                    		<input type="hidden" name="command" value="add_item_to_basket" /> 
               	 		</form>
                		<c:if test="${sessionScope.client.status == 1}">
	                  		<form action="Controller" method="post">
                    			<input class="btn btn-block btn-primary" type="submit" 
                    					value="<fmt:message key="button.item.modify" />" /> 
                    			<input type="hidden" name="item_id" value="${item.itemId}" />
                    			<input type="hidden" name="command" value="to_modify_item" />
                  			</form>
                  			<form action="Controller" method="post">
                    			<input class="btn btn-block btn-danger" type="submit" 
                    					value="<fmt:message key="button.item.delete" />" /> 
                    			<input type="hidden" name="command" value="delete_item" />
                    			<input type="hidden" name="item_id" value="${item.itemId}" />
                  			</form>
                		</c:if>
					</div>
              	</div> 
			</div>       
		</c:forEach>          
	</div>
	<c:forEach begin="1" end="${numberOfPages}" var="i">
    	<c:choose>
        	<c:when test="${currentPage eq i}">
            	<!--  do nothing -->
            </c:when>
            <c:otherwise>
            	<ul class="pagination">
                	<li>
                    	<form class="form" method="POST" action="Controller">
                    		<input type="hidden" name="pagenum" value="${i}" />
							<input type="hidden" name="command" value="take_items" /> 
							<input class="btn btn-lg btn-link btn-block" type="submit" 
									name="take_items" value="${i}" />
						</form>
					</li>
				</ul>
            </c:otherwise>
       	</c:choose>
	</c:forEach>
</div>   
</body>
</html>

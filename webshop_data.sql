-- MySQL dump 10.13  Distrib 5.7.15, for Linux (x86_64)
--
-- Host: localhost    Database: webshop
-- ------------------------------------------------------
-- Server version	5.7.15-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES ('admin',926252727,'603c4f970680b6b4396f939b6553c5f778da827cab1a5b9254700a7d1838f67a','4g7fp1ocoiikn1p0cjg3jvr2sq','System','Administrator',1,0);
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` VALUES (64535297,'Panasonic Lumix DMC-GH4 Body',453,'41 999 000 ?','???????????????? ????????????? ??????. ??????? ???????????? ???????? ???????? ??????????? ?????? 4K-????? ? ????????? 100 ????/?. ??????? ???????? ? ??????? ?? ?????????? ?????? ? ??????? ?? ????????? ????? ? ????. ?????? ???????? 16 ??. Live MOS-?????? ????????? Four Thirds ? ??????????? ????????? Venus Engine IX. ??????????? ??????? ?? 12 ??????/? ??? 7 ??????/? ?? ???????? ????????????. ?????????????? ??????????? ???????? ????? ???????? ?????? ????????, ??????? ?????????? Depth-of-Focus (??????? ????????). ??????????? ????? ?????????? 3\" ????????? OLED-????? ??????????? 1 036 000 ????????, ? ????? OLED-???????????? ??????????? 2 360 000 ?????. ??? ?? ?????? ???????? ?????????? Wi-Fi-????????????, NFC-?????, HDMI-???????? ? ?????? ????????????, ??? ???????, shotgun-???????? ? ?????????? ????.','images/cameras/panasonic.jpg','5'),(73538518,'Sony KD-55X8509C',436,'38 001 600 ?.','55\" 3840x2160, ??????? VA, ?????? ?????????? ???? 1000, 3D, Smart TV, Wi-Fi','images/tvs/sony.jpg','3'),(106382495,'Packard Bell EasyNote TE69BH-37YW',299,'7 991 000 ?.','15.6\" 1366 x 768, Intel Core i3 5005U 2000 ???, 4 ??, 500 ?? (HDD), Intel HD Graphics 5500, Linux, ???? ?????? ?????, ???? ??????? ?????','images/notebooks/packardbell.png','1'),(107274907,'Dell XPS 15 9550 [9550-1370]',149,'58 706 000 ?','15.6\" 3840 x 2160 ?????????, ?????????, Intel Core i7 6700HQ 2600 ???, 16 ??, 512 ?? (SSD), NVIDIA GeForce GTX 960M, Windows 10, ???? ?????? ???????????, ???? ??????? ??????','images/notebooks/dell.jpg','1'),(114972335,'Panasonic TX-50CXR700',830,'22 027 000 ?','50\" 3840x2160, ??????? VA, ?????? ?????????? ???? 200, Smart TV, Wi-Fi','images/tvs/panasonic.jpg','1'),(166111464,'Xiaomi Redmi Note 2 16GB Black',987,'3 800 000 р','Android, экран 5.5\" IPS (1080x1920), ОЗУ 2 ГБ, флэш-память 16 ГБ, карты памяти, камера 13 Мп, аккумулятор 3060 мАч, Dual SIM, цвет черный','images/phones/xiaomi.jpg','1'),(237661416,'Canon EOS 5D Mark III Body',352,'64 062 700 р','зеркальная камера, матрица Full frame 22.3 Мп, без объектива (body)','images/cameras/canon.jpg','5'),(253231719,'MacBook Pro 13\'\' Retina (MF839)',666,'34 300 000 р.','13.3\" 2560 x 1600 глянцевый, Intel Core i5 2700 МГц, 8 ГБ, 128 Гб (SSD), Intel Iris Graphics 6100, OS X, цвет крышки серебристый, цвет корпуса серебристый','images/notebooks/macbook15.jpg','1'),(294344334,'HP Spectre 13-4102dx x360 [N5R94UA]',150,'28 900 000 р','13.3\" 2560 x 1440 глянцевый, сенсорный, Intel Core i7 5500U 2400 МГц, 8 ГБ, 512 Гб (SSD), Intel HD Graphics 520, Windows 10, цвет крышки серебристый, цвет корпуса серебристый','images/notebooks/hp.jpg','1'),(427595677,'Apple iPad Pro 9.7 128GB Silver',129,'17 480 000 р','9.7\" IPS (2048x1536), iOS, флэш-память 128 ГБ, цвет серебристый','images/tablets/apple.jpg','4'),(456852765,'Fujifilm Instax Mini 8',213,'1 699 000 р.','Компактная аналоговая камера с возможностью мгновенной печати на пленке Instax Mini (62 x 46 мм). Модель представлена в пяти цветовых вариантах: черный, белый, розовый, голубой и желтый. Имеется 4 режима съемки (регулировка относительного отверстия F12.7 - F32), функция Hi-Key для получения ярких цветов, встроенная вспышка, видоискатель.','images/cameras/fujifilm.jpg','5'),(477273438,'LG 105UC9V',342,'1 456 000 000 р','105\" 5120x2160, матрица IPS, экв. частота 1300 Hz, 3D, Smart TV, Wi-Fi','images/tvs/lg.jpg','3'),(530880113,'Samsung Galaxy Tab 3 10.1 16GB Jet Black',462,'6 800 000 р','10.1\" PLS (1280x800), Android, ОЗУ 1 ГБ, флэш-память 16 ГБ, цвет черный','images/tablets/samsung.jpg','4'),(561664557,'Sony a7 Body (ILCE-7)',456,'22 395 000 р.','беззеркальная камера, матрица Full frame 24.3 Мп, без объектива (body), Wi-Fi','images/cameras/sony.jpg','5'),(628366644,'Sony Xperia Z5 Compact Graphite Black',657,'9 500 000 р','Android, экран 4.6\" IPS (720x1280), ОЗУ 2 ГБ, флэш-память 32 ГБ, карты памяти, камера 23 Мп, аккумулятор 2700 мАч, цвет черный','images/phones/sony.jpg','2'),(639963241,'Nikon Coolpix P900',321,'10 900 000 р','компакт-камера, матрица 1/2.3\" 16 Мп, объектив 83X F2.8-6.5 24-2000 мм, Wi-Fi','images/cameras/nikon.jpg','1'),(734048752,'Lenovo IdeaPad 100s-11IBY [80R2004YRK]',150,'5 192 000 р','11.6\" 1366 x 768, Intel Atom Z3735F 1330 МГц, 2 ГБ, 32 Гб (eMMC), Intel HD Graphics, Windows 10, цвет крышки голубой, цвет корпуса белый','images/notebooks/lenovo.jpg','1'),(762682190,'BBK 43LEX-5007/FT2C',128,'8 480 000 р.','43\" 1920x1080, матрица IPS, Smart TV, Wi-Fi','images/tvs/bbk.jpg','3'),(767190679,'Leica S (Typ 007) Body Black',139,'419 999 000 р','Топ-модель системы Leica S. По сравнению с предыдущей камерой обновился сенсор CMOS и процессор Leica Maestro II. Leica S значительно ускоряет и упрощает фотографический процесс с множеством функций, которые являются уникальными для среднеформатной фотографии - и некоторые из этих функций эксклюзивны.','images/cameras/leica.jpg','5'),(815231044,'Huawei P8 Lite Dual Black',983,'3 990 000 ?','Android, ????? 5\" IPS (720x1280), ??? 2 ??, ????-?????? 16 ??, ????? ??????, ?????? 13 ??, ??????????? 2200 ???, Dual SIM, ???? ??????','images/phones/huawei.jpg','2'),(953122796,'Lenovo Tab 2 A7-20F 8GB Black [59444653]',362,'1 572 600 р','7.0\" IPS (1024x600), Android, ОЗУ 1 ГБ, флэш-память 8 ГБ, цвет черный','images/tablets/lenovo.jpg','4'),(973316137,'ASUS Zenbook Pro UX501VW-FI109R',150,'35 900 000 р','15.6\" 3840 x 2160 матовый, Intel Core i7 6700HQ 2600 МГц, 16 ГБ, 512 Гб (SSD), NVIDIA GeForce GTX 960M, Windows 10 Pro, цвет крышки серебристый, цвет корпуса серебристый','images/notebooks/asus.jpg','1'),(1017948462,'Samsung Galaxy S7 32GB Black Onyx [G930F]',1003,'12 900 000 р','Android, экран 5.1\" AMOLED (1440x2560), ОЗУ 4 ГБ, флэш-память 32 ГБ, карты памяти, камера 12 Мп, аккумулятор 3000 мАч, цвет черный','images/phones/samsung.jpg','2'),(1105319577,'Horizont 42LE7218D',356,'7 799 900 р','42\" 1920x1080, матрица LCD, Smart TV, Wi-Fi','images/tvs/horizont.jpg','3'),(1142202974,'Samsung NX mini Body',999,'6 290 000 р.','Любительская беззеркальная камера в очень тонком корпусе. Аппарат получил 1\" 20 Мп. сенсор с технологией обратной засветки BSI и поворотный 3-дюймовый IPS-дисплей. Модель предлагает диапазон выдержек от 1/16 000 до 30 с, светочувствительность ISO 100—25 600, имеется поддержка серийной съемки (6 кадров в секунду), записи Full HD-видео при 30 fps, RAW-формата. Камера оборудована модулем Wi-Fi и чипом NFC, а также интерфейсами HDMI и USB 2.0. Комплект без объектива.','images/cameras/samsung.jpg','5'),(1181238595,'Lenovo P70-A',586,'4 500 000 р','Android, экран 5\" IPS (720x1280), ОЗУ 2 ГБ, флэш-память 16 ГБ, карты памяти, камера 13 Мп, аккумулятор 4000 мАч, Dual SIM, цвет синий','images/phones/lenovo.jpg','2'),(1191516305,'Leica M Edition 60 Kit 35mm',234,'329_999_000 р','Камера премиум-класса. Выглядит как классическая плёночная камера Leica, на тыльной стороне нет экрана, а все настройки осуществляются с помощью регуляторов на корпусе. Всего выпущено 600 моделей. Аппарат оснащен полнокадровым 24 Мп. CMOS-сенсором и выполнен в металлическом корпусе. Изображения сохраняются в формате RAW и доступны только с SD-карты — выходов USB или HDMI нет.','images/cameras/leica1.jpg','5'),(1292320482,'MSI GT80S 6QE-053RU Titan SLI Heroes SE',230,'94 550 000 р.','18.4\" 1920 x 1080 матовый, Intel Core i7 6820HK 2700 МГц, 32 ГБ, 1000 ГБ + 256 Гб (HDD + SSD), NVIDIA GeForce GTX 980M, Windows 10, цвет крышки черный, цвет корпуса черный','images/notebooks/msi.jpg','1'),(1348071258,'DEXP Atlas H178',27,'6 231 000 р','15.6\" 1366 x 768 глянцевый, Intel Pentium 3825U 1900 МГц, 4 ГБ, 500 Гб (HDD), NVIDIA GeForce 930M, без ОС, цвет крышки серый, цвет корпуса серый','images/notebooks/dexp.jpg','1'),(1441339929,'ASUS Transformer Book T100HA-FU002T 32GB',196,'7 492 500 р','10.1\" (1280x800), Windows 10, ОЗУ 2 ГБ, флэш-память 32 ГБ, цвет темно-серый','images/tablets/asus.jpg','4'),(1505480352,'Samsung UE40JU6000U',643,'12 590 000 р','40\" 3840x2160, матрица VA, индекс динамичных сцен 1000, Smart TV, Wi-Fi','images/tvs/samsung.jpg','3'),(1527563616,'Prestigio MultiPad Wize 3341 8GB 3G',826,'2 999 900 р.','10.1\" IPS (1280x800), Android, ОЗУ 1 ГБ, флэш-память 8 ГБ, 3G, цвет черный','images/tablets/prestigio.jpg','4'),(1564740725,'Huawei MediaPad M2 10.0 Premium 64GB LTE',54,'10 038 400 р','10.1\" IPS (1920x1200), Android, ОЗУ 3 ГБ, флэш-память 64 ГБ, LTE, цвет золотистый','images/tablets/huawei.jpg','4'),(1568048348,'Toshiba Satellite L70-B-10W',70,'11 400 000 р','17.3\" 1600 x 900, Intel Core i3 4005U 1700 МГц, 4 ГБ, 750 Гб (HDD), AMD Radeon R7 M260, Windows 8.1, цвет крышки золотистый, цвет корпуса золотистый/черный','images/notebooks/toshiba.jpg','1'),(1570648566,'Xiaomi Mi Pad 7.9 Mi515 16GB White',234,'6 300 000 р','7.9\" IPS (2048x1536), Android, ОЗУ 2 ГБ, флэш-память 16 ГБ, цвет черный/белый','images/tablets/xiaomi.jpg','4'),(1634452265,'LG Nexus 5X 16GB Carbon',879,'6 690 000 р','Android, экран 5.2\" IPS (1080x1920), ОЗУ 2 ГБ, флэш-память 16 ГБ, камера 12 Мп, аккумулятор 2700 мАч, цвет черный','images/phones/nexus.jpg','2'),(1637912621,'Apple iPhone SE 16GB Space Gray',670,'10 990 000 р','Apple iOS, экран 4\" IPS (640x1136), ОЗУ 2 ГБ, флэш-память 16 ГБ, камера 12 Мп, аккумулятор 1642 мАч, цвет темно-серый','images/phones/iphone.jpg','2'),(1701489995,'Philips 50PFT6510',654,'14 361 100 р','50\" 1920x1080, матрица VA, индекс динамичных сцен 800, 3D, Smart TV, Wi-Fi','images/tvs/phillips.jpg','3'),(1778574531,'Toshiba 32S3633DG',367,'6 899 900 р.','32\" 1366x768, Smart TV, Wi-Fi','images/tvs/toshiba.jpg','3'),(1822173543,'Sony Xperia Z4 Tablet 32GB LTE (SGP771RU/B)',555,'10 957 300 р','10.1\" IPS (2560x1600), Android, ОЗУ 3 ГБ, флэш-память 32 ГБ, LTE, цвет черный','images/tablets/sony.jpg','4'),(2013429219,'Nokia 230 Dual SIM Black',350,'1 799 000 р','экран 2.8\" TFT (240x320), ОЗУ 16 МБ, карты памяти, камера 2 Мп, аккумулятор 1200 мАч, Dual SIM, цвет серый/черный','images/phones/nokia.jpg','2'),(2071628452,'HTC Desire 626G White',443,'9 000 001 р','Android, экран 5\" IPS (720x1280), ОЗУ 1 ГБ, флэш-память 8 ГБ, карты памяти, камера 13 Мп, аккумулятор 2000 мАч, Dual SIM, цвет белый','images/phones/htc.jpg','2'),(2099249116,'Acer Aspire V Nitro VN7-592G [NX.G6JEP.005]',160,'20 342 000 р.','15.6\" 1920 x 1080 матовый, Intel Core i7 6700HQ 2600 МГц, 8 ГБ, 1000 ГБ (HDD), NVIDIA GeForce GTX 960M, без ОС, цвет крышки черный, цвет корпуса черный','images/notebooks/acer.jpg','1');
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `item_order`
--

LOCK TABLES `item_order` WRITE;
/*!40000 ALTER TABLE `item_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-01 20:25:25

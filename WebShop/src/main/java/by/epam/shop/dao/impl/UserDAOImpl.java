package by.epam.shop.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.shop.dao.UserDAO;
import by.epam.shop.dao.pool.DBConnectionPool;
import by.epam.shop.entity.User;
import by.epam.shop.exception.ConnectionPoolException;
import by.epam.shop.exception.DAOException;

public class UserDAOImpl implements UserDAO {

	private static final Logger LOG = Logger.getLogger(UserDAOImpl.class);
	private static final String CLIENT_LOGIN = "client.login";
	private static final String CLIENT_PASSWORD = "client.password";
	private static final String CLIENT_SALT = "client.salt";
	private static final String CLIENT_ID = "client.id";
	private static final String CLIENT_NAME = "client.name";
	private static final String CLIENT_SURNAME = "client.surname";
	private static final String CLIENT_STATUS = "client.is_admin";
	private static final String CLIENT_STATUS_BL = "client.is_black";
	private static final String SQL_SIGNUP_CLIENT = "INSERT INTO webshop.client(id, login, password, salt, name, surname, is_admin, is_black) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String SQL_IS_CLIENT_EXISTS = "SELECT * FROM webshop.client WHERE BINARY login = ?";
	private static final String SQL_FIND_CLIENT_BY_LOGIN = "SELECT * FROM webshop.client WHERE BINARY login = ?";
	private static final String SQL_FIND_ALL_CLIENTS = "SELECT * FROM webshop.client";
	private static final String SQL_DELETE_CLIENT = "DELETE FROM webshop.client WHERE BINARY client.login = ?";
	private static final String SQL_UPDATE_BLACKLIST_STATUS = "UPDATE webshop.client SET is_black = ? WHERE BINARY client.login = ?";
	private static UserDAOImpl instance = new UserDAOImpl();
	
	private UserDAOImpl() {
	}

	public static UserDAO getInstance() {
		return instance;
	}

	@Override
	public List<User> findAll() throws DAOException {
		DBConnectionPool pool = DBConnectionPool.getInstance();

		ArrayList<User> users = new ArrayList<User>();
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.giveConnection();
			prStatement = connection.prepareStatement(SQL_FIND_ALL_CLIENTS);
			ResultSet resultSet = prStatement.executeQuery();
			while (resultSet.next()) {
				User user = new User();
				user.setLogin(resultSet.getString(CLIENT_LOGIN));
				user.setPassword(resultSet.getString(CLIENT_PASSWORD));
				user.setSalt(resultSet.getString(CLIENT_SALT));
				user.setId(resultSet.getInt(CLIENT_ID));
				user.setName(resultSet.getString(CLIENT_NAME));
				user.setSurname(resultSet.getString(CLIENT_SURNAME));
				user.setStatus(resultSet.getInt(CLIENT_STATUS));
				user.setStatusBL(resultSet.getInt(CLIENT_STATUS_BL));

				users.add(user);
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException();
		} finally {
			try {
				if (prStatement != null) {
					prStatement.close();
				}
				pool.takeConnection(connection);
				connection.close();
			} catch (SQLException | ConnectionPoolException e) {
				throw new DAOException(e);
			}
		}
		return users;
	}

	public boolean create(User user) throws DAOException {
		LOG.info("ClientDAO.signUp()");

		DBConnectionPool pool = DBConnectionPool.getInstance();

		boolean flag = false;
		if (exists(user.getLogin()) || user == null) {
			LOG.info(2);
			return flag;
		}
		LOG.info(3);
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.giveConnection();
			LOG.info(4);
			prStatement = connection.prepareStatement(SQL_SIGNUP_CLIENT);
			LOG.info(5);
			prStatement.setInt(1, user.getId());
			prStatement.setString(2, user.getLogin());
			prStatement.setString(3, user.getPassword());
			prStatement.setString(4, user.getSalt());
			prStatement.setString(5, user.getName());
			prStatement.setString(6, user.getSurname());
			prStatement.setInt(7, user.getStatus());
			prStatement.setInt(8, user.getStatusBL());
			if (prStatement.executeUpdate() > 0) {
				flag = true;
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
					prStatement.close();
				}
				pool.takeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
				throw new DAOException(e);
			}
		}
		return flag;
	}

	public boolean exists(String login) throws DAOException {
		DBConnectionPool pool = DBConnectionPool.getInstance();

		boolean result = false;
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.giveConnection();
			prStatement = connection.prepareStatement(SQL_IS_CLIENT_EXISTS);
			prStatement.setString(1, login);
			ResultSet resultSet;
			resultSet = prStatement.executeQuery();
			result = resultSet.first();
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException();
		} finally {
			try {
				if (prStatement != null) {
					prStatement.close();
				}
				pool.takeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
				throw new DAOException(e);
			}
		}
		return result;
	}

	public User findEntity(String login) throws DAOException {
		DBConnectionPool pool = DBConnectionPool.getInstance();

		User user = null;
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.giveConnection();
			prStatement = connection.prepareStatement(SQL_FIND_CLIENT_BY_LOGIN);
			prStatement.setString(1, login);
			ResultSet resultSet = prStatement.executeQuery();
			if (resultSet.next()) {
				user = new User();
				user.setLogin(resultSet.getString(CLIENT_LOGIN));
				user.setPassword(resultSet.getString(CLIENT_PASSWORD));
				user.setSalt(resultSet.getString(CLIENT_SALT));
				user.setName(resultSet.getString(CLIENT_NAME));
				user.setSurname(resultSet.getString(CLIENT_SURNAME));
				user.setStatus(resultSet.getInt(CLIENT_STATUS));
				user.setId(resultSet.getInt(CLIENT_ID));
				user.setStatusBL(resultSet.getInt(CLIENT_STATUS_BL));
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
					prStatement.close();
				}
				pool.takeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
				throw new DAOException(e);
			}
		}
		return user;
	}

	@Override
	public boolean delete(String login) throws DAOException {
		DBConnectionPool pool = DBConnectionPool.getInstance();

		boolean result = false;
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.giveConnection();
			prStatement = connection.prepareStatement(SQL_DELETE_CLIENT);
			prStatement.setString(1, login);
			if (prStatement.executeUpdate() > 0) {
				result = true;
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
					prStatement.close();
				}
				pool.takeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
				throw new DAOException(e);
			}
		}
		return result;
	}

	public boolean changeBlackListStatus(String login, int status) throws DAOException {
		DBConnectionPool pool = DBConnectionPool.getInstance();

		boolean result = false;
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.giveConnection();
			prStatement = connection.prepareStatement(SQL_UPDATE_BLACKLIST_STATUS);
			prStatement.setInt(1, status);
			prStatement.setString(2, login);
			if (prStatement.executeUpdate() == 1) {
				result = true;
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
					prStatement.close();
				}
				pool.takeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
				throw new DAOException(e);
			}
		}
		return result;
	}
}

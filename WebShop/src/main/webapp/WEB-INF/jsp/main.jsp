<%@ page language="java" contentType="text/html; charset=UTF-8"
		pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<%@include file="/WEB-INF/jsp/jspf/bundle.jspf"%>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><fmt:message key="title.main" /></title>
 	<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
  	<script src="${pageContext.request.contextPath}/js/jquery-1.12.4.min.js"></script>
	<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
  			rel="stylesheet">
	<link href="${pageContext.request.contextPath}/css/main.css"
  			rel="stylesheet">
</head>
<body>
	<%@include file="/WEB-INF/jsp/jspf/ind_nav.jspf" %>
  	<%@include file="/WEB-INF/jsp/jspf/message.jspf"%>

<div class="container">
	<h4 align="center" id="message">
		<ctg:info name="${sessionScope.client.name}" surname="${sessionScope.client.surname}" locale="${sessionScope.locale}"/>
	</h4>
</div>

<div class="container">
	<div class="row">
		<div class="col-sm-4">
        	<div class="panel panel-primary">
          		<div class="panel-heading"><fmt:message key="static.notebooks"/></div>
          		<div class="panel-body">
            		<img src="${pageContext.request.contextPath}/images/notebooks/notebook.jpg"
              				class="img-responsive" alt="Image">
          		</div>
          		<form action="Controller" method="post">    
                	<input type="hidden" name="command" value="take_items_by_type" />
                  	<input type="hidden" name="item_type" value="1" />
                  	<input class="btn btn btn-primary" type="submit" name="take_items" 
                  			value="<fmt:message key="static.more"/>" />
                </form>
        	</div>
      	</div>      
      	<div class="col-sm-4">
        	<div class="panel panel-primary">
         		<div class="panel-heading"><fmt:message key="static.phones"/></div>
          		<div class="panel-body">
            		<img src="${pageContext.request.contextPath}/images/phones/phone.jpg"
              				class="img-responsive" alt="Image">
          		</div>
          		<form action="Controller" method="post">    
                		<input type="hidden" name="command" value="take_items_by_type" />
                  		<input type="hidden" name="item_type" value="2" />
                  		<input class="btn btn btn-primary" type="submit" name="take_items" value="<fmt:message key="static.more"/>" />
                </form>
        	</div>
      	</div>
      	<div class="col-sm-4">
        	<div class="panel panel-primary">
          		<div class="panel-heading"><fmt:message key="static.tvs"/></div>
          		<div class="panel-body">
            		<img src="${pageContext.request.contextPath}/images/tvs/tv.jpg"
              		class="img-responsive" alt="Image">
          		</div>
          		<form action="Controller" method="post">    
					<input type="hidden" name="command" value="take_items_by_type" />
                  	<input type="hidden" name="item_type" value="3" />
                  	<input class="btn btn btn-primary" type="submit" name="take_items" value="<fmt:message key="static.more"/>" />
                </form>
        	</div>
      	</div>
      	<div class="col-sm-4">
        	<div class="panel panel-primary">
          		<div class="panel-heading"><fmt:message key="static.tablets"/></div>
          		<div class="panel-body">
            		<img src="${pageContext.request.contextPath}/images/tablets/tablet.jpg"
            				class="img-responsive" alt="Image">
          		</div>
          		<form action="Controller" method="post">    
                	<input type="hidden" name="command" value="take_items_by_type" />
                  	<input type="hidden" name="item_type" value="4" />
                  	<input class="btn btn btn-primary" type="submit" name="take_items" value="<fmt:message key="static.more"/>" />
                </form>
        	</div>
      	</div>
      	<div class="col-sm-4">
        	<div class="panel panel-primary">
          		<div class="panel-heading"><fmt:message key="static.cameras"/></div>
          		<div class="panel-body">
            		<img src="${pageContext.request.contextPath}/images/cameras/camera.jpg"
            				class="img-responsive" alt="Image">
          		</div>
          		<form action="Controller" method="post">    
                	<input type="hidden" name="command" value="take_items_by_type" />
                  	<input type="hidden" name="item_type" value="5" />
                  	<input class="btn btn btn-primary" type="submit" name="take_items" value="<fmt:message key="static.more"/>" />
                </form>
        	</div>
      	</div>
	</div>
</div>
</body>
</html>

package by.epam.shop.entity;

import java.io.Serializable;

/**
 * The base class for all entities involved in business logic.
 */
public abstract class AbstractEntity implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

}

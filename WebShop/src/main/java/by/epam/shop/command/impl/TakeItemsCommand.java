package by.epam.shop.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.shop.command.Command;
import by.epam.shop.dao.ItemDAO;
import by.epam.shop.dao.impl.ItemDAOImpl;
import by.epam.shop.entity.Item;
import by.epam.shop.exception.DAOException;
import by.epam.shop.resource.ConfigurationManager;
import by.epam.shop.resource.MessageManager;

public class TakeItemsCommand implements Command {

	private static final Logger LOG = Logger.getLogger(TakeItemsCommand.class);
	private final String MESSAGE = "message";
	private final String URL = "url";
	private final String ITEMS = "items";
	private final String NUMBER_OF_PAGES = "numberOfPages";
	private final String CURRENT_PAGE = "currentPage";
	private final String ITEMS_PAGE = "path.page.items";
	private final String ERROR_PAGE = "path.page.error";
	private final int itemsPerPage = 9;
	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("TakeItemsCommand.execute()");

		String page = null;
		ItemDAO itemDAO = ItemDAOImpl.getInstance();
		int pagenum = 1;
		
        if(request.getParameter("pagenum") != null)
            pagenum = Integer.parseInt(request.getParameter("pagenum"));
		try {
			List<Item> items = itemDAO.findLimited((pagenum - 1)*itemsPerPage, itemsPerPage);
			if (items.isEmpty()) {
				request.setAttribute(MESSAGE, MessageManager.SEARCHING_ITEM_ERROR);
				page = ConfigurationManager.getProperty(ERROR_PAGE);
			} else {
				int numberOfItems = itemDAO.calcNumberOfItems();
		        int numberOfPages = (int) Math.ceil(numberOfItems * 1.0 / itemsPerPage);
		        request.setAttribute(NUMBER_OF_PAGES, numberOfPages);
		        request.setAttribute(CURRENT_PAGE, page);
				request.getSession().setAttribute(ITEMS, items);
				page = ConfigurationManager.getProperty(ITEMS_PAGE);
			}
		} catch (DAOException e) {
			LOG.error("DAOException", e);
			request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
			page = ConfigurationManager.getProperty(ERROR_PAGE);
		}
		request.getSession().setAttribute(URL, page);
		return page;
	}
}
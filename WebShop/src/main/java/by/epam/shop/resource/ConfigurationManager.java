package by.epam.shop.resource;

import java.util.ResourceBundle;

/**
 * Provides access to application configuration.
 */
public class ConfigurationManager {

	private final static String PROPERTY_FILE = "config";
	private final static ResourceBundle resourceBundle = ResourceBundle.getBundle(PROPERTY_FILE);

	private ConfigurationManager() {
	}

	public static String getProperty(String key) {
		return resourceBundle.getString(key);
	}
}
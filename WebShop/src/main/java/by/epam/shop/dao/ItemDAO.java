package by.epam.shop.dao;

import java.util.List;

import by.epam.shop.entity.Item;
import by.epam.shop.exception.DAOException;

public interface ItemDAO extends GenericDAO<Integer, Item> {

	/**
	 * Returns a list of all available items from the data source.
	 */
	public List<Item> findAll() throws DAOException;

	/**
	 * Finds specified amount of items in data source.
	 * 
	 * @param offset
	 *            position of cursor in data source.
	 * @param amount
	 *            number of entities to return.
	 * @return List of entities.
	 * @throws DAOException
	 */
	public List<Item> findLimited(int offset, int amount) throws DAOException;

	/**
	 * @return a number of items, available in data source.
	 */
	public int calcNumberOfItems() throws DAOException;

	/**
	 * Searches in the data source for an item by identifier.
	 */
	public Item findEntity(Integer id) throws DAOException;

	/**
	 * Deletes an item from the data source by it's identifier.
	 */
	public boolean delete(Integer id) throws DAOException;

	/**
	 * Saves given item in the data source
	 */
	public boolean create(Item item) throws DAOException;

	/**
	 * Finds item by it's type.
	 * 
	 * @param type
	 * @return
	 * @throws DAOException
	 */
	public List<Item> findItemsByType(String type) throws DAOException;

	/**
	 * Updates item info in the data source.
	 * 
	 * @param item
	 * @return true if item is successfully update, otherwise false.
	 * @throws DAOException
	 */
	public boolean modifyItemInfo(Item item) throws DAOException;

	/**
	 * Finds all items contained in the order by it's id.
	 * 
	 * @param id
	 * @throws DAOException
	 */
	public List<Item> findItemsByOrderId(int id) throws DAOException;
}

<%@ page language="java" contentType="text/html; charset=UTF-8"
		pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/WEB-INF/jsp/jspf/bundle.jspf"%>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><fmt:message key="title.main" /></title>
 	<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
  	<script src="${pageContext.request.contextPath}/js/jquery-1.12.4.min.js"></script>
	<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
  			rel="stylesheet">
	<link href="${pageContext.request.contextPath}/css/main.css"
  			rel="stylesheet">
</head>
<body>
	<%@include file="/WEB-INF/jsp/jspf/ind_nav.jspf" %>
    <%@include file="/WEB-INF/jsp/jspf/message.jspf"%>
    
<div class="container">
	<div class="row">
    	<c:forEach var="order" items="${sessionScope.orders}">
			<div class="col-sm-12">
            	<div class="panel panel-primary">
            		<div class="panel-heading">
            			<fmt:message key="info.order"/> <fmt:message key="info.order.id"/> ${order.orderId}
            		</div>
            		<div class="panel-body">
						<p><fmt:message key="info.order.date"/> ${order.date}</p>
                  		<hr>
                  		<p><fmt:message key="info.items"/></p>
						<div class="container-fluid" >    
                    		<c:forEach var="item" items="${order.items}"> 
                      			<div class="col-sm-4">
                      				<div class="panel panel-primary">
                      					<div class="panel-heading">
                      						<fmt:message key="info.item.name"/> ${item.itemName}
                      					</div>
                            			<div class="panel-body">                           
                            				<p><fmt:message key="info.item.desc"/> ${item.itemDescription}</p>
                            				<p><fmt:message key="info.item.price"/> ${item.price}</p>                        
                          				</div>
                        			</div>
                     			</div>
                   	 		</c:forEach>
						</div>
                   		<c:choose>
                    		<c:when test="${order.status == 1}">
                      			<fmt:message key="info.order_delivered" />
                    		</c:when>
                    		<c:otherwise>
                    			<fmt:message key="info.order_not_delivered" />
                      			<form action="Controller" method="post" >
                        			<input class="btn btn-danger" type="submit" 
                        					value="<fmt:message key="button.cancel_order" />" />
                        			<input type="hidden" name="command"  value="delete_order" />
                        			<input type="hidden" name="order_id" value="${order.orderId}" /> 
                        		</form>
                    		</c:otherwise>
                  		</c:choose> 
    				</div> 
          		</div>
     		</div>
		</c:forEach>                  
	</div>
</div>  
</body>
</html>

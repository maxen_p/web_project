package by.epam.shop.dao;

import java.util.List;

import by.epam.shop.entity.AbstractEntity;
import by.epam.shop.exception.DAOException;

/**
 * GenericDAO is the base interface for all DAO interfaces and implementations.
 * DAO(Data Access Object) is an object that provides an abstract interface to
 * some type of database or other persistence mechanism.
 *
 * @param <K>
 *            Key or identifier of an entity.
 * @param <T>
 *            Entity.
 */
public interface GenericDAO<K, T extends AbstractEntity> {

	/**
	 * Returns a list of all available entities from data source.
	 * 
	 * @throws DAOException
	 */
	public List<T> findAll() throws DAOException;

	/**
	 * Searches in data source for an entity by identifier.
	 * 
	 * @param id
	 *            identifier of an entity.
	 * @return entity, if one exists.
	 * @throws DAOException
	 */
	public T findEntity(K id) throws DAOException;

	/**
	 * Deletes an entity from data source by identifier.
	 * 
	 * @param id
	 *            identifier of an entity
	 * @return true if operation is successful, false otherwise.
	 * @throws DAOException
	 */
	public boolean delete(K id) throws DAOException;

	/**
	 * Saves given entity in the data source.
	 * 
	 * @param entity
	 * @return true, if entity is successfully saved.
	 * @throws DAOException
	 */
	public boolean create(T entity) throws DAOException;

}

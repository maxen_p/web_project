package by.epam.shop.command.admin;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.shop.command.Command;
import by.epam.shop.dao.ItemDAO;
import by.epam.shop.dao.impl.ItemDAOImpl;
import by.epam.shop.entity.Item;
import by.epam.shop.exception.DAOException;
import by.epam.shop.resource.ConfigurationManager;
import by.epam.shop.resource.MessageManager;
import by.epam.shop.util.Validator;

public class ModifyItemCommand implements Command {

	private static final Logger LOG = Logger.getLogger(ModifyItemCommand.class);
	private final String URL = "url";
	private final String NAME = "name";
	private final String AMOUNT = "amount";
	private final String MANUFACTER = "manufacter";
	private final String DESCRIPTION = "description";
	private final String PATH = "path";
	private final String ITEM_ID = "item_id";
	private final String TYPE = "type";
	private final String MESSAGE = "message";
	private final String ITEM = "newITEM";
	private final String ADMIN_MODIFY_PAGE = "path.page.admin.modify";

	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("ModifyItem.execute()");

		String page = null;
		String itemName = request.getParameter(NAME).trim();
		String amount = request.getParameter(AMOUNT).trim();
		String manufacter = request.getParameter(MANUFACTER).trim();
		String description = request.getParameter(DESCRIPTION).trim();
		String path = request.getParameter(PATH).trim();
		String type = request.getParameter(TYPE).trim();

		if (!Validator.checkEmptyField(itemName, amount, manufacter, description, path) || type == null) {
			request.setAttribute(MESSAGE, MessageManager.ADD_ITEM_ERROR);
			page = ConfigurationManager.getProperty(ADMIN_MODIFY_PAGE);
			request.getSession().setAttribute(URL, page);
			return page;
		}

		Item item = new Item();
		item.setItemName(itemName);
		item.setItemType(type);
		item.setItemsAmount(Integer.parseInt(amount));
		item.setPrice(manufacter);
		item.setItemPicPath(path);
		item.setItemDescription(description);
		item.setItemId(Integer.parseInt(request.getParameter(ITEM_ID)));

		String check = Validator.checkItemInfo(item);
		ItemDAO itemDAO = ItemDAOImpl.getInstance();
		try {
			if (check == null) {
				LOG.info("item.toString():" + item.toString());
				if (itemDAO.modifyItemInfo(item)) {
					request.setAttribute(MESSAGE, MessageManager.UPDATE_ITEM_SUCCESS);
					page = ConfigurationManager.getProperty(ADMIN_MODIFY_PAGE);
					request.setAttribute(ITEM, item);
				} else {
					request.setAttribute(MESSAGE, MessageManager.ERROR_UPDATE_ITEM_INFO);
					page = ConfigurationManager.getProperty(ADMIN_MODIFY_PAGE);
				}
			} else {
				request.setAttribute(MESSAGE, MessageManager.ILLEGAL_ITEM_INPUT_DATA);
				page = ConfigurationManager.getProperty(ADMIN_MODIFY_PAGE);
			}
		} catch (DAOException e) {
			LOG.error("DAOException", e);
			request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
			page = ConfigurationManager.getProperty(ADMIN_MODIFY_PAGE);
		}
		request.getSession().setAttribute(URL, page);
		return page;
	}
}

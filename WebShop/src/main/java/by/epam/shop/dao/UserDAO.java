package by.epam.shop.dao;

import java.util.List;

import by.epam.shop.entity.User;
import by.epam.shop.exception.DAOException;

public interface UserDAO extends GenericDAO<String, User> {

	/**
	 * Finds all users in data source.
	 */
	public List<User> findAll() throws DAOException;

	/**
	 * Deletes user form data source.
	 */
	public boolean delete(String login) throws DAOException;

	/**
	 * Saves user in the data source.
	 */
	public boolean create(User user) throws DAOException;

	/**
	 * Checks if user is already created.
	 * 
	 * @param login
	 * @return true if user is already created, otherwise false.
	 * @throws DAOException
	 */
	public boolean exists(String login) throws DAOException;

	/**
	 * Finds user by given login.
	 */
	public User findEntity(String login) throws DAOException;

	/**
	 * Tries to change user's blacklist status.
	 * 
	 * @param login
	 * @param status
	 * @return true if status is changed, otherwise false.
	 * @throws DAOException
	 */
	public boolean changeBlackListStatus(String login, int status) throws DAOException;

}

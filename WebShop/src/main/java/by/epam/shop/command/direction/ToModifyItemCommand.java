package by.epam.shop.command.direction;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.shop.command.Command;
import by.epam.shop.dao.ItemDAO;
import by.epam.shop.dao.OrderDAO;
import by.epam.shop.dao.impl.ItemDAOImpl;
import by.epam.shop.dao.impl.OrderDAOImpl;
import by.epam.shop.entity.Item;
import by.epam.shop.exception.DAOException;
import by.epam.shop.resource.ConfigurationManager;
import by.epam.shop.resource.MessageManager;

public class ToModifyItemCommand implements Command {

	private static final Logger LOG = Logger.getLogger(ToModifyItemCommand.class);
	private final String URL = "url";
	private final String MESSAGE = "message";
	private final String ITEM = "item";
	private final String ITEM_ID = "item_id";
	private final String AMOUNT_MAP = "amount_map";
	private final String ITEMS_PAGE = "path.page.items";
	private final String ADMIN_MODIFY_PAGE = "path.page.admin.modify";
	private final String ERROR_PAGE = "path.page.error";

	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("ToModifyItemCommand.execute()");

		String page = null;
		int itemId = Integer.parseInt(request.getParameter(ITEM_ID));
		HashMap<Integer, Integer> amountMap = (HashMap<Integer, Integer>) request.getSession().getAttribute(AMOUNT_MAP);
		ItemDAO itemDAO = ItemDAOImpl.getInstance();
		OrderDAO orderDAO = OrderDAOImpl.getInstance();
		try {
			if (orderDAO.findOrdersByItemId(itemId)
					|| amountMap != null && !amountMap.isEmpty() && amountMap.containsKey(itemId)) {
				request.setAttribute(MESSAGE, MessageManager.ITEM_IN_ORDER);
				page = ConfigurationManager.getProperty(ITEMS_PAGE);
				return page;
			}

			Item item = itemDAO.findEntity(itemId);
			if (item != null) {
				request.getSession().setAttribute(ITEM, item);
				page = ConfigurationManager.getProperty(ADMIN_MODIFY_PAGE);
			} else {
				request.setAttribute(MESSAGE, MessageManager.MODIFY_ITEM_ERROR);
				page = ConfigurationManager.getProperty(ITEMS_PAGE);
			}
		} catch (DAOException e) {
			LOG.error(e);
			request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
			page = ConfigurationManager.getProperty(ERROR_PAGE);
		}
		request.getSession().setAttribute(URL, page);
		return page;
	}
}
